## CLion

Toolchain:
C Compiler = /<emsdk_install>/emsdk/upstream/emscripten/emcc
C++ Compiler = /<emsdk_install>/emsdk/upstream/emscripten/em++

CMake options:
-DCMAKE_CROSSCOMPILING_EMULATOR=/<emsdk_install>/emsdk/node/14.18.2_64bit/bin/node -DCMAKE_TOOLCHAIN_FILE=/<emsdk_install>/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake

Environment:
CC=/<emsdk_install>/emsdk/upstream/emscripten/emcc;CXX=/<emsdk_install>/emsdk/upstream/emscripten/em++;AR=/<emsdk_install>/emsdk/upstream/emscripten/emar;LD=/<emsdk_install>/emsdk/upstream/emscripten/emcc;NM=/<emsdk_install>/emsdk/upstream/bin/llvm;LDSHARED=/<emsdk_install>/emsdk/upstream/emscripten/emcc;LLVM_ROOT=/<emsdk_install>/emsdk/upstream/bin;EMSCRIPTEN_TOOLS=/<emsdk_install>/emsdk/upstream/emscripten/tools;HOST_CC=/<emsdk_install>/emsdk/upstream/emscripten/emcc;HOST_CXX=/<emsdk_install>/emsdk/upstream/emscripten/em++;HOST_CFLAGS=-W;HOST_CXXFLAGS=-W;PKG_CONFIG_LIBDIR=/<emsdk_install>/emsdk/upstream/emscripten/system/lib/pkgconfig;PKG_CONFIG_PATH=;EMSCRIPTEN=/<emsdk_install>/emsdk/upstream/emscripten
