#!/usr/bin/env bash

UPLOAD_HOST=${UPLOAD_HOST:-$1}
UPLOAD_PORT=${UPLOAD_PORT:-$2}
USER_CREDS=${USER_CREDS:-$3}

cd bin-out || exit 1
mkdir -p bin-out
mkdir -p lib-out

cp libcxx_calendar.a lib-out/
cp calendar_tests bin-out/
cp calendar_property_tests bin-out/
cp calendar_combinatorial_tests bin-out/

VERSION=$(awk '/full/ {print $8}' ../src/version.h | sed 's/"//g' | sed 's/;//g')

if [ -z "$UPLOAD_HOST" ]; then
	echo "UPLOAD_HOST not set, unable to upload!"
	exit 1
fi

if [ -z "$UPLOAD_PORT" ]; then
  echo "UPLOAD_PORT not set, unable to upload!"
  exit 1
fi

if [ -z "$USER_CREDS" ]; then
	echo "USER_CREDS not set, unable to upload!"
	exit 1
fi


BASE_URL="http://${UPLOAD_HOST}:${UPLOAD_PORT}/repository/binary"

cd bin-out || exit 2
for f in *
do
  echo "Uploading ${f} with version ${VERSION} to ${BASE_URL}/cpp/calendar/bin/${VERSION}/${f}"
	curl -v -u $USER_CREDS --upload-file ${f} "${BASE_URL}/cpp/calendar/bin/${VERSION}/${f}"
done

cd ../lib-out || exit 2
for f in *
do
	echo "Uploading ${f} with version ${VERSION} to ${BASE_URL}/cpp/calendar/lib/${VERSION}/${f}"
	curl -v -u $USER_CREDS --upload-file ${f} "${BASE_URL}/cpp/calendar/lib/${VERSION}/${f}"
done
exit 0