#!/usr/bin/env bash

UPLOAD_HOST=${UPLOAD_HOST:-$1}
UPLOAD_PORT=${UPLOAD_PORT:-$2}
USER_CREDS=${USER_CREDS:-$3}

if [ -z "$UPLOAD_HOST" ]; then
	echo "UPLOAD_HOST not set, unable to upload!"
	exit 1
fi

if [ -z "$UPLOAD_PORT" ]; then
  echo "UPLOAD_PORT not set, unable to upload!"
  exit 1
fi

if [ -z "$USER_CREDS" ]; then
	echo "USER_CREDS not set, unable to upload!"
	exit 1
fi

printf "\n\nCleaning...\n\n"
bash clean.sh
printf "\n\nCollecting Version Info...\n\n"
bash setup-version-info.sh
printf "\n\nGenerating docs...\n\n"
bash ./build-scripts/docs.sh

VERSION=$(awk '/full/ {print $8}' src/version.h | sed 's/"//g' | sed 's/;//g')

BASE_URL="http://${UPLOAD_HOST}:${UPLOAD_PORT}/repository/binary"

cd docs-out || exit 2
for f in *
do
  echo "Uploading ${f} with version ${VERSION} to ${BASE_URL}/cpp/calendar/docs/${VERSION}/${f}"
	curl -v -u $USER_CREDS --upload-file ${f} "${BASE_URL}/cpp/calendar/docs/${VERSION}/${f}"
done