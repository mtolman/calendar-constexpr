## CLion not detecting build chain

Add the following to /etc/wsl.conf (sets up auto-mounting of windows folders)

```
# /etc/wsl.conf
[automount]
options = "metadata"
enabled = true
```

Then restart the VMs by running `wsl.exe -t <distro>`
