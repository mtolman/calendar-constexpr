## TODO (not in book)

* Add IANA database support:
  * Add daylight savings calculations based on IANA database.
  * Add timezone information from IANA database
    * Location-based lookup
    * Timezone offsets

## TODO (in book)

* Roman nomenclature (pg 77)
* Julian Seasons (pg 83)
* Coptic names (pg 89)
* Ethiopian names (pg 91)
* Islamic names
* Old Hindu names (pg 156, 157, 159)
