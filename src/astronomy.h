#pragma once

#include "common.h"

namespace calendars::astronomy {
  struct TimeOffset {
    int hours;
    int minutes = 0;
    int seconds = 0;

    CAL_CONSTEXPR_FN operator double() const noexcept {
      return (static_cast<double>(hours) + (static_cast<double>(minutes) + static_cast<double>(seconds) / 60.0) / 60.0) / 24.0;
    }
  };

  struct DegreeMinuteSeconds {
    int degrees;
    int minutes;
    int seconds;

    CAL_CONSTEXPR_FN operator double() const noexcept {
      return (static_cast<double>(degrees) + (static_cast<double>(minutes) + static_cast<double>(seconds) / 60.0) / 60.0) / 24.0;
    }
  };

  struct EarthPosition {
    double latitude;
    double longitude;
    double elevationMeters;
    double utcOffsetDayFraction;
  };

  namespace places {
    static constexpr EarthPosition urbana = EarthPosition {40.1, -88.2, 225, TimeOffset{-6}};
    static constexpr EarthPosition greenwhich = EarthPosition {51.4777815, 0, 46.9, 0};
    static constexpr EarthPosition mecca = EarthPosition{DegreeMinuteSeconds{21, 25, 24}, DegreeMinuteSeconds{39, 49, 24}, 298, TimeOffset{3}};
    static constexpr EarthPosition jerusalem = EarthPosition{31.78, 25.24, 740, TimeOffset{2}};
    static constexpr EarthPosition acre = EarthPosition{32.94, 25.09, 22, TimeOffset{2}};
  }
}
