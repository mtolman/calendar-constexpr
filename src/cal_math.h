#pragma once

#include <cinttypes>
#include <cmath>
#include <tuple>
#include <array>

#include "common.h"

/**
 * Collection of constexpr math functions used in calendrical calculations
 */
namespace calendars::math {

  /**
   * Floors a number
   * Is a const-constexpr function which allows compile-time optimizations,
   * however it has a runtime performance hit since it is a software implementation of floor and not a hardware implementation
   * @tparam T
   * @tparam R
   * @param num
   * @return
   */
  template<typename T, typename R = double>
  constexpr R floor_constexpr(T num) noexcept {
    // Check to see if we're at our maximum supported radix range
    // If we are, then the number we received is an integer since all the significant digits
    //    have to be positive at this point
    const T max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
    if (num >= max || num <= -max) {
      return static_cast<R>(num);
    }
    const auto a = static_cast<T>(static_cast<int64_t>(num));
    return (num < 0 && num != a) ? static_cast<R>(a - 1) : static_cast<R>(a);
  }

  /**
   * Ceil a number
   * Is a const-constexpr function which allows compile-time optimizations,
   * however it has a runtime performance hit since it is a software implementation of floor and not a hardware implementation
   * @tparam T
   * @tparam R
   * @param num
   * @return
   */
  template<typename T, typename R = double>
  constexpr R ceil_constexpr(T num) noexcept {
    // Check to see if we're at our maximum supported radix range
    // If we are, then the number we received is an integer since all the significant digits
    //    have to be positive at this point
    const T max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
    if (num >= max || num <= -max) {
      return num;
    }
    const auto a = static_cast<T>(static_cast<int64_t>(num));
    return (num > 0 && num != a) ? static_cast<R>(a + 1) : static_cast<R>(a);
  }

  /**
   * Floors a number
   * @tparam T
   * @param num
   * @return
   */
  template<typename T, typename R = double>
  CAL_CONSTEXPR_FN R floor(T num) noexcept {
    if constexpr (std::is_integral<T>::value) {
      return static_cast<R>(num);
    }
#if CAL_USE_STD_IS_CONST_EVAL
    // Ideal case, using the C++20 standard
    if (std::is_constant_evaluated()) {
#elif CAL_USE_BUILTIN_IS_CONST_EVAL
    // Fallback case, using compiler built-ins
    // Note: some IDEs will say this if statement is constant,
    // However, that is because they look at only the runtime execution of the function,
    //  not both the runtime and compile time execution of the function
    if (__builtin_is_constant_evaluated()) {
#else
    // Forced constexpr case
    if constexpr (CAL_USE_CONSTEXPR) {
#endif
      return floor_constexpr<T, R>(num);
    }
    else {
      return static_cast<R>(std::floor(num));
    }
  }

  /**
   * Ceils a number
   * Is a const-constexpr function which allows compile-time optimizations
   * @tparam T
   * @param num
   * @return
   */
  template<typename T, typename R = double>
  CAL_CONSTEXPR_FN R ceil(T num) noexcept {
    if constexpr (std::is_integral<T>::value) {
      return num;
    }
#if CAL_USE_CONSTEXPR
#ifdef __cpp_lib_is_constant_evaluated
    if (std::is_constant_evaluated()) {
#endif
      return static_cast<R>(ceil_constexpr(num));
#ifdef __cpp_lib_is_constant_evaluated
    }
    else {
      return static_cast<R>(std::ceil(num));
    }
#endif
#else
    else {
      return static_cast<R>(std::ceil(num));
    }
#endif
  }

  /**
   * Returns the sign of a number
   * Is a const-constexpr function which allows compile-time optimizations
   * @tparam T
   * @param num
   * @return
   */
  template<typename T, typename R = T>
  CAL_CONSTEXPR_FN R sign(T num) noexcept {
    return num > 0 ? 1 : num < 0 ? -1 : 0;
  }

  /**
   * Returns the absolute value of a number
   * Is a const-constexpr function which allows compile-time optimizations
   * @tparam T
   * @param num
   * @return
   */
  template<typename T, typename R = T>
  CAL_CONSTEXPR_FN R abs(T num) noexcept {
#if CAL_USE_STD_IS_CONST_EVAL
    // Ideal case, using the C++20 standard
    if (std::is_constant_evaluated()) {
#elif CAL_USE_BUILTIN_IS_CONST_EVAL
    // Fallback case, using compiler built-ins
    // Note: some IDEs will say this if statement is constant,
    // However, that is because they look at only the runtime execution of the function,
    //  not both the runtime and compile time execution of the function
    if (__builtin_is_constant_evaluated()) {
#else
    // Forced constexpr case
    if constexpr (CAL_USE_CONSTEXPR) {
#endif
      return num < 0 ? -num : num;
    }
    else {
      return static_cast<R>(std::abs(num));
    }
  }

  /**
   * Rounds a number
   * @tparam T
   * @param num
   * @return
   */
  template<typename T, typename R = T>
  CAL_CONSTEXPR_FN R round(T num) noexcept {
    // Return integer values as-is
    if constexpr (std::is_integral<T>::value) {
      return static_cast<R>(num);
    }

    // Check to see if we're at our maximum supported radix range
    // If we are, then the number we received is an integer since all the significant digits
    //    have to be positive at this point
    const T max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));

    // Get the absolute value for the comparison check (we have to get it if the check fails
    // anyways)
    const auto a = abs(num);
    if (a >= max) {
      return static_cast<R>(num);
    }

    // Round the number (with halfway being away from zero)
    return static_cast<R>(sign(num) * floor(a + 0.5));
  }

  /**
   * x mod [0..y)
   * @tparam T input type
   * @tparam R range type
   * @param x input
   * @param y range end
   * @return
   */
  template<typename T, typename R>
  CAL_CONSTEXPR_FN T mod(T x, R y) noexcept {
    return static_cast<T>(x - y * floor(static_cast<double>(x) / static_cast<double>(y)));
  }

  /**
   * x mod [1..y]
   * @tparam T input type
   * @tparam R range type
   * @param x input
   * @param y range end
   * @return
   */
  template<typename T>
  CAL_CONSTEXPR_FN T max(T x, T y) noexcept {
    return x > y ? x : y;
  }

  /**
   * x mod [1..y]
   * @tparam T input type
   * @tparam R range type
   * @param x input
   * @param y range end
   * @return
   */
  template<typename T, typename R>
  CAL_CONSTEXPR_FN T amod(T x, R y) noexcept {
    return static_cast<T>(y + mod(x, -y));
  }

  /**
   * x mod [a..b)
   * @tparam T param type
   * @param x input
   * @param a range start
   * @param b range end
   * @return
   */
  template<typename T>
  CAL_CONSTEXPR_FN T mod_range(T x, int64_t a, int64_t b) noexcept {
    return a == b ? x : a + mod(x - a, b - a);
  }

  /**
   * x mod [1..b]
   * @tparam T param type
   * @param x input
   * @param b range
   * @return
   */
  template<typename T>
  CAL_CONSTEXPR_FN T adjusted_rem_fun(T x, int64_t b) noexcept {
    auto m = mod(x, b);
    return m == 0 ? b : m;
  }

  /**
   * Gets the greatest common denominator
   * @param x
   * @param y
   * @return
   */
  CAL_CONSTEXPR_FN int64_t gcd(int64_t x, int64_t y) noexcept {
    if (y == 0) {
      return x;
    }
    else {
      return gcd(y, mod(x, y));
    }
  }

  /**
   * Gets the least common multiple
   * @param x
   * @param y
   * @return
   */
  CAL_CONSTEXPR_FN int64_t lcm(int64_t x, int64_t y) noexcept { return x * y / gcd(x, y); }

  template<int... Nums>
  struct Radix {};

  template<typename Y, typename A>
  struct YxA;

  /**
   * Takes a Radix for year cycles and approximate number of days in the cycle, and approximates the number of days elapsed
   * @tparam YC Year cycles
   * @tparam AC Approximate days per Cycle
   * @tparam Ys List of Year cycles
   * @tparam As List of Approximate days
   */
  template<template<int...> typename YC, template<int...> typename AC, int... Ys, int... As>
  struct YxA<YC<Ys...>, AC<As...>> {
    static_assert(sizeof...(Ys) + 1 == sizeof...(As), "Invalid dimensions given; As must be 1 larger than Ys");

    /**
     * Sums all of the approximate days that have occurred in a number of years (assumes day 0 is the start of year 0)
     * @param year The year number to count
     * @return
     */
    static CAL_CONSTEXPR_FN int64_t sum(int64_t year) {
      const std::array<int, sizeof...(Ys)> yearDivs             = {Ys...};
      const std::array<int, sizeof...(As)> segmentMultiplicands = {As...};
      std::array<int, sizeof...(Ys) + 1>   yearSegments         = {Ys...};
      for (size_t i = 0; i < yearDivs.size(); ++i) {
        yearSegments.at(yearSegments.size() - 1 - i) = static_cast<int>(math::mod(year, yearDivs.at(yearDivs.size() - 1 - i)));
        year                                         = year / yearDivs.at(yearDivs.size() - 1 - i);
      }
      yearSegments[ 0 ] = static_cast<int>(year);

      int64_t total = 0;
      for (size_t i = 0; i < yearSegments.size(); ++i) {
        total += yearSegments.at(i) * segmentMultiplicands.at(i);
      }
      return total;
    }
  };
}  // namespace calendars::math
