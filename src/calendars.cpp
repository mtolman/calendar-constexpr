#include "calendars.h"

#include "cal_math.h"

static constexpr int64_t                  epoch                    = 0;
[[maybe_unused]] static constexpr int64_t akanEpoch                = 37 - epoch;
[[maybe_unused]] static constexpr int64_t armenianEpoch            = 201443 - epoch;
[[maybe_unused]] static constexpr int64_t babylonianEpoch          = -113502 - epoch;
[[maybe_unused]] static constexpr int64_t bahaiEpoch               = 673222 - epoch;
[[maybe_unused]] static constexpr int64_t chineseEpoch             = -963099 - epoch;
[[maybe_unused]] static constexpr int64_t copticEpoch              = 103605 - epoch;
[[maybe_unused]] static constexpr int64_t egyptianEpoch            = -272787 - epoch;
[[maybe_unused]] static constexpr int64_t ethiopicEpoch            = 2796 - epoch;
[[maybe_unused]] static constexpr int64_t frenchRevolutionaryEpoch = 654415 - epoch;
[[maybe_unused]] static constexpr int64_t gregorianEpoch           = 1 - epoch;
[[maybe_unused]] static constexpr int64_t hebrewEpoch              = -1373427 - epoch;
[[maybe_unused]] static constexpr int64_t hinduEpoch               = -1132959 - epoch;
[[maybe_unused]] static constexpr int64_t islamicEpoch             = 227015 - epoch;
[[maybe_unused]] static constexpr int64_t isoEpoch                 = 1 - epoch;
[[maybe_unused]] static constexpr int64_t julianEpoch              = -1 - epoch;
[[maybe_unused]] static constexpr int64_t kaliYugaEpoch            = -1132959 - epoch;
[[maybe_unused]] static constexpr int64_t mayanEpoch               = -1137142 - epoch;
[[maybe_unused]] static constexpr int64_t persianEpoch             = 226896 - epoch;
[[maybe_unused]] static constexpr int64_t samaritanEpoch           = -598573 - epoch;
[[maybe_unused]] static constexpr int64_t tibetanEpoch             = -46410 - epoch;
[[maybe_unused]] static constexpr int64_t unixEpoch                = 719163 - epoch;
[[maybe_unused]] static constexpr int64_t zoroastrianEpoch         = 230638 - epoch;


std::vector<calendars::Date> calendars::Date::positions_in_range(int64_t pth_moment, int64_t c_day_cycle, int64_t delta, Date start, const Date& end) {
  auto res       = std::vector<Date>();
  auto startDate = start.to<Date>();
  while (true) {
    Date date{Date{static_cast<double>(math::mod_range(pth_moment - delta, startDate.day, startDate.day + c_day_cycle))}};
    if (date >= end) {
      return res;
    }

    res.push_back(date);
    startDate.day += c_day_cycle;
  }
}


#if CAL_USE_CONSTEXPR
// Series of static asserts to make sure const constexpr evaluation works and isn't broken
// Only enabled of constexpr is enabled
static_assert((calendars::Date{9445}.to<calendars::AkanDay>().prefix) == 6);
static_assert((calendars::Date{9445}.to<calendars::ArmenianDate>().year) == -526);
static_assert((calendars::Date{9445}.to<calendars::CopticDate>().year) == -257);
static_assert((calendars::Date{9445}.to<calendars::EgyptianDate>().year) == 774);
static_assert((calendars::Date{9445}.to<calendars::EthiopianDate>().year) == 19);
static_assert((calendars::Date{9445}.to<calendars::HebrewDate>().year) == 3787);
static_assert((calendars::Date{9445}.to<calendars::IcelandicDate>().year) == 26);
static_assert((calendars::Date{9445}.to<calendars::IslamicDate>().year) == -613);
static_assert((calendars::Date{9445}.to<calendars::IsoDate>().year) == 26);
static_assert((calendars::Date{9445}.to<calendars::MicroUnixTimestamp>().seconds) == -61319635200);
static_assert((calendars::Date{9445}.to<calendars::ModifiedJulianDay>().datetime) == -669131);
static_assert((calendars::Date{9445}.to<calendars::Moment>().datetime) == 9445);
static_assert((calendars::Date{9445}.to<calendars::Date>().day) == 9445);
static_assert((calendars::Date{9445}.to<calendars::UnixTimestamp>().seconds) == -61319635200);
static_assert((calendars::Date{9445}.to<calendars::ZoroastrianDate>().year) == -606);
static_assert((calendars::Date{9445}.to<calendars::OldHinduSolarDate>().year) == 3127);
static_assert((calendars::Date{9445}.to<calendars::OldHinduLuniSolarDate>().year) == 3127);
static_assert(calendars::Date{9445}.to<calendars::GregorianDate>().year == 26, "Gregorian date constexpr conversion failed");
static_assert(calendars::Date{9445}.to<calendars::JulianDate>().year == 26, "JulianDate date constexpr conversion failed");
static_assert(calendars::Date{9445}.to<calendars::JulianDay>().datetime == 1730869.5, "JulianDay date constexpr conversion failed");
static_assert((calendars::Date{calendars::Moment{9445.42}}.to<calendars::JulianDay>().datetime) == 1730869.5);
static_assert((calendars::Date{calendars::Moment{9445.42}}.to<calendars::UnixTimestamp>().seconds) == -61319635200);
static_assert((calendars::Date{calendars::Moment{9445.42}}.to<calendars::MicroUnixTimestamp>().seconds) == -61319635200);
static_assert((calendars::Date{calendars::Moment{9445.42}}.to<calendars::ModifiedJulianDay>().datetime) == -669131);
static_assert((calendars::Date{calendars::Moment{9445.42}}.to<calendars::Moment>().datetime) == 9445);
static_assert((calendars::Date{9445}.add_days(23).to<calendars::Date>().day) == 9468);
static_assert((calendars::Date{9445}.sub_days(23).to<calendars::Date>().day) == 9422);
static_assert((calendars::Date{9445}.day_difference(calendars::Date{calendars::Date{942}})) == 8503);
static_assert(static_cast<int>(calendars::Date{9445}.day_of_week()) == 2);
static_assert(calendars::Date{9445}.day_of_m_cycle(14, 2) == 7);
static_assert(calendars::Date{9445}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::MONDAY).to<calendars::Date>().day == 9444);
static_assert(calendars::Date{9445}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::MONDAY).to<calendars::Date>().day == 9451);
static_assert(calendars::Date{9445}.day_of_week_nearest(calendars::DAY_OF_WEEK::MONDAY).to<calendars::Date>().day == 9444);
static_assert(calendars::Date{9445}.day_of_week_before(calendars::DAY_OF_WEEK::MONDAY).to<calendars::Date>().day == 9444);
static_assert(calendars::Date{9445}.day_of_week_after(calendars::DAY_OF_WEEK::MONDAY).to<calendars::Date>().day == 9451);
static_assert(calendars::Date{9445}.kth_day_of_m_cycle_on_or_before(2, 35, 8).to<calendars::Date>().day == 9425);
static_assert(calendars::Date{9445}.kth_day_of_m_cycle_before(2, 35, 8).to<calendars::Date>().day == 9425);
static_assert(calendars::Date{9445}.kth_day_of_m_cycle_after(2, 35, 8).to<calendars::Date>().day == 9460);
static_assert(calendars::Date{9445}.kth_day_of_m_cycle_on_or_after(2, 35, 8).to<calendars::Date>().day == 9460);
static_assert(calendars::Date{9445}.kth_day_of_m_cycle_nearest_to(2, 35, 8).to<calendars::Date>().day == 9460);
static_assert(calendars::Date{9445}.nth_week_day(2, calendars::DAY_OF_WEEK::TUESDAY).to<calendars::Date>().day == 9452);
#endif
