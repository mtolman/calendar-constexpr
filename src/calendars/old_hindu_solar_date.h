#pragma once

#include "common.h"

namespace calendars {
  
  /**
   * Arithmetical implementation of old Hindu Solar calendars that was based on
   *  mean astronomical events rather than true astronomical events.
   * Additionally, this calendars is based on the Kali Yuga ("Iron Age") epoch.
   * The expired year is the number of years that have elapsed since the onset
   *  of the Kali Yuga.
   *
   * Do note that while the Old Hindu Solar calendars was primarily used before 1100 B.C.,
   *  no attempt is made to ensure historical accuracy with historical records.
   */
  struct OldHinduSolarDate {
    static constexpr int64_t epoch            = -1132959;
    static constexpr double  aryaSolarYear    = 15779175.0 / 43200.0;
    static constexpr double  aryaSolarMonth   = aryaSolarYear / 12.0;
    static constexpr double  aryaJovianPeriod = 1577917500.0 / 364224.0;

    enum class SAURA
    {
      ARIES,
      TAURUS,
      GEMINI,
      CANCER,
      LEO,
      VIRGO,
      LIBRA,
      SCORPIO,
      SAGITTARIUS,
      CAPRICORN,
      AQUARIUS,
      PISCES
    };

    using MONTH = SAURA;

    int64_t year;
    SAURA   month;
    int16_t day;

    CAL_CONSTEXPR_CLASS_FN OldHinduSolarDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN OldHinduSolarDate(int64_t year, SAURA saura, int16_t day) noexcept : year(year), month(saura), day(day) {}
    CAL_CONSTEXPR_CLASS_FN OldHinduSolarDate(int64_t year, int16_t saura, int16_t day) noexcept
        : OldHinduSolarDate(year, static_cast<SAURA>(math::abs(math::mod(saura, 12))), math::abs(day)) {}

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t day_count(const Date& date) {
      return date.sub_days(calendars::OldHinduSolarDate::epoch).day;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN int64_t jovian_year(const Date& date) {
      return math::mod_range(27 + math::floor<double, int64_t>(static_cast<double>(day_count(date)) / (aryaJovianPeriod / 12.0)), 1, 60);
    }

    CAL_DEF_OPS_CONSTEXPR(OldHinduSolarDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const OldHinduSolarDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN OldHinduSolarDate from_date(const Date& date) noexcept {
      const auto sun   = static_cast<double>(OldHinduSolarDate::day_count(date)) + 6.0 / 24.0;
      const auto hYear = math::floor<double, int64_t>(sun / OldHinduSolarDate::aryaSolarYear);
      const auto hMonth =
          static_cast<OldHinduSolarDate::SAURA>(math::mod(math::floor<double, int64_t>(sun / OldHinduSolarDate::aryaSolarMonth), 12) + 1);
      const auto jDay = static_cast<int16_t>(math::floor(math::mod(sun, OldHinduSolarDate::aryaSolarMonth)) + 1);
      return OldHinduSolarDate{hYear, hMonth, jDay};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{math::ceil<double, int64_t>(static_cast<double>(OldHinduSolarDate::epoch)
                                              + static_cast<double>(year) * OldHinduSolarDate::aryaSolarYear
                                              + (static_cast<double>(month) - 1) * OldHinduSolarDate::aryaSolarMonth + day - 30.0 / 24.0)};
    }
  };
}
