#pragma once

#include "common.h"

namespace calendars {
  
  /**
   * Represents a date on the Egyptian calendars.
   * The Egyptian calendars has 13 months, with the first 12 months being 30 days and the last month
   * (the epagomenae) being 5 days. Note that the epagomenae (month 13) does not have a name.
   */
  struct EgyptianDate {
    static constexpr int64_t epoch = -272787;

    /**
     * Year of the date
     */
    int64_t year = 0;
    /**
     * MONTH of the year (1-13)
     */
    int16_t month = 1;
    /**
     * Day of the month: 1-30 (epagomenae 1-5)
     */
    int16_t day = 1;

    CAL_CONSTEXPR_CLASS_FN EgyptianDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN EgyptianDate(int64_t year, int16_t month, int16_t day) noexcept
        : year(year), month(math::abs(math::amod(month, 13))), day(math::abs(day)) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const EgyptianDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(EgyptianDate)
    
    [[nodiscard]] static CAL_CONSTEXPR_FN EgyptianDate from_date(const Date& rdDate) noexcept {
      const auto days          = rdDate.day - EgyptianDate::epoch;
      const auto egyptianYear  = math::floor<double, int64_t>(static_cast<double>(days) / 365.0) + 1;
      const auto egyptianMonth = math::floor<double, int64_t>(math::mod(static_cast<double>(days), 365) / 30.0) + 1;
      const auto egyptianDay   = days - 365 * (egyptianYear - 1) - 30 * (egyptianMonth - 1) + 1;
      return EgyptianDate{static_cast<decltype(EgyptianDate::year)>(egyptianYear),
                          static_cast<decltype(EgyptianDate::month)>(egyptianMonth),
                          static_cast<decltype(EgyptianDate::day)>(egyptianDay)};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{EgyptianDate::epoch + ((365 * (year - 1) + 30 * static_cast<int64_t>(month - 1) + static_cast<int64_t>(day - 1)))};
    }
  };
}
