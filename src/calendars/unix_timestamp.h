#pragma once

#include "common.h"

namespace calendars {
  /**
   * A Unix Timestamp which represents the number of seconds elapsed since January 1, 1970
   * (Gregorian) in UTC. Seconds are represented by an integer and will be rounded to the nearest
   * whole integer.
   */
  struct UnixTimestamp {
    /**
     * Number of seconds since January 1, 1970
     */
    int64_t                  seconds;
    static constexpr int64_t epoch = 719163;

    CAL_CONSTEXPR_CLASS_FN UnixTimestamp() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN UnixTimestamp(int64_t seconds) noexcept : seconds(seconds) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const UnixTimestamp& o) const noexcept { return seconds < o.seconds; }

    CAL_DEF_OPS_CONSTEXPR(UnixTimestamp)

    [[nodiscard]] static CAL_CONSTEXPR_FN UnixTimestamp from_moment(const Moment& moment) noexcept {
      return UnixTimestamp{static_cast<int64_t>(math::round(24 * 60 * 60 * (moment.datetime - static_cast<double>(UnixTimestamp::epoch))))};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      return Moment{UnixTimestamp::epoch + static_cast<double>(seconds) / (24 * 60 * 60.0)};
    }
  };
}
