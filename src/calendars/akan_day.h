#pragma once

#include "common.h"

namespace calendars {
  /**
   * Represents an Akan Day
   * Unlike Gregorian days which have a single 7-day cycle,
   * Akan days have a dual cycle with the first cycle being 6 long and the second being 7 long.
   * When multiplied together, it gives us a 42-length total cycle for akan days.
   */
  struct AkanDay {
    int prefix = 0;
    int stem   = 0;

    static constexpr int          epoch       = 37;
    static CAL_CONSTEXPR_VAR auto cycleLength = 42;

    /**
     * Creates an AkanDay from a prefix and stem
     * @param prefix
     * @param stem
     */
    CAL_CONSTEXPR_CLASS_FN AkanDay(int prefix, int stem) noexcept : prefix(math::amod(prefix, 6)), stem(math::amod(stem, 7)) {}

    /**
     * Creates an Akan day from a day count
     * @param n
     */
    CAL_CONSTEXPR_CLASS_FN explicit AkanDay(int n) noexcept : prefix(math::amod(n, 6)), stem(math::amod(n, 7)) {}

    /**
     * Creates an Akan day from a day count
     * @param n
     */
    CAL_CONSTEXPR_CLASS_FN explicit AkanDay(int64_t n) noexcept
        : prefix(static_cast<int>(math::amod(n, 6))), stem(static_cast<int>(math::amod(n, 7))) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator==(const AkanDay& o) const noexcept { return prefix == o.prefix && stem == o.stem; }
    [[nodiscard]] CAL_CONSTEXPR_FN bool operator!=(const AkanDay& o) const noexcept { return !(*this == o); }

    [[nodiscard]] static CAL_CONSTEXPR_FN AkanDay from_date(const Date& rdDate) noexcept {
      return AkanDay{static_cast<int>(rdDate.day - AkanDay::epoch)};
    }

    /**
     * returns the difference between two AkanDays
     * @param n
     */
    CAL_CONSTEXPR_CLASS_FN int operator-(const AkanDay& other) const noexcept {
      const auto prefixDiff = other.prefix - prefix;
      const auto stemDiff   = other.stem - stem;
      return math::amod(prefixDiff + 36 * (stemDiff - prefixDiff), 42);
    }
  };

  /**
   * Returns the Date which corresponds to the Akan Day on or before the provided Date
   * @param akanDay Akan Day
   * @return
   */
  [[nodiscard]] CAL_CONSTEXPR_FN Date akan_day_on_or_before(const Date& rdDate, const AkanDay& akanDay) noexcept {
    return Date{math::mod_range(Date{0}.to<AkanDay>() - akanDay, rdDate.day, rdDate.day - 42)};
  }
}