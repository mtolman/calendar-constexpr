#pragma once

#include "common.h"

namespace calendars {
  
  /**
   * Date on the Islamic calendars
   * Note that this is the arithmetical Islamic calendar and is not the same as the observed Islamic calendars.
   * The observed Islamic calendars is determined by religious authorities. This code is only an
   * approximation, and is not an authoritative source for the Islamic calendars.
   *
   * Additionally, all Islamic holidays are approximations and may vary from actual observances.
   */
  struct IslamicDate {
    enum class MONTH : int16_t
    {
      MUHARRAM = 1,
      SAFAR,
      RABI_I,
      RABI_II,
      JUMADA_I,
      JUMADA_II,
      RAJAB,
      SHABAN,
      RAMADAN,
      SHAWWAL,
      DHU_AL_QADA,
      DHU_AL_HIJJA
    };

    static constexpr int64_t epoch = 227015;
    int64_t                  year;
    MONTH                    month;
    int16_t                  day;

    CAL_CONSTEXPR_CLASS_FN IslamicDate() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN IslamicDate(int64_t year, MONTH month, int16_t day) noexcept : year(year), month(month), day(day) {}
    CAL_CONSTEXPR_CLASS_FN IslamicDate(int64_t year, int16_t month, int16_t day) noexcept
        : IslamicDate(year, static_cast<MONTH>(math::abs(math::amod(month, 12))), math::abs(day)) {}

    [[nodiscard]] static CAL_CONSTEXPR_FN bool is_leap_year(int64_t year) noexcept { return math::mod(14 + 11 * year, 30) < 11; }

    CAL_DEF_OPS_CONSTEXPR(IslamicDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const IslamicDate& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN IslamicDate from_date(const Date& date) noexcept {
      const auto year      = math::floor<double, int64_t>(static_cast<double>(30 * (date.day - IslamicDate::epoch) + 10646) / 10631.0);
      const auto priorDays = date.day_difference(Date::from(IslamicDate{year, IslamicDate::MONTH::MUHARRAM, 1}));
      const auto month     = math::floor<double, IslamicDate::MONTH>(static_cast<double>(priorDays * 11 + 330) / 325.0);
      return IslamicDate{year, month, static_cast<int16_t>(date.day_difference(Date::from(IslamicDate{year, month, 1})) + 1)};
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{IslamicDate::epoch - 1 + (year - 1) * 354
                  + math::floor<double, int64_t>(static_cast<double>(3 + 11 * year) / 30.0) + 29 * (static_cast<int64_t>(month) - 1)
                  + math::floor<double, int64_t>(static_cast<double>(month) / 2.0) + day};
    }
  };
}
