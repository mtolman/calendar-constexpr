#pragma once

#include "common.h"

/**
 * Root namespace for all calendars
 */
namespace calendars {

  /**
   * Represents a Julian day Date (not to be confused with a Julian Date).
   * A Julian day date represents the number of days since January 1, 4713 B.C.E.
   * However, unlike most other calendaring systems which start their days at midnight, the Julian
   * day date starts it's days at Noon. Consequently, the decimal portion of all datetimes in a
   * Julian day date are offset by 0.5 from the corresponding Date.
   *
   * JdDates are often used for astronomy.
   *
   * Moment will be used when converting with a JulianDay.
   */
  struct JulianDay {
    /**
     * Number of days since Noon on January 1, 4713 B.C.E.
     */
    double                  datetime;
    static constexpr double epoch = -1721424.5;

    CAL_CONSTEXPR_CLASS_FN JulianDay() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN explicit JulianDay(double datetime) noexcept : datetime(datetime) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const JulianDay& o) const noexcept { return datetime < o.datetime; }

    CAL_DEF_OPS_CONSTEXPR(JulianDay)

    [[nodiscard]] static CAL_CONSTEXPR_FN JulianDay from_moment(const Moment& moment) noexcept {
      return JulianDay{moment.datetime - JulianDay::epoch};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      return Moment{datetime + JulianDay::epoch};
    }
  };
}