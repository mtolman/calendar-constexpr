#pragma once

#include "long_date.h"

namespace calendars::mayan {
  /**
   * Represents a Mayan Haab which tracks the Haab month and day, but no year.
   * This means we can convert to the Haab, but we cannot convert from the Haab.
   */
  struct Haab {
    enum class MONTH
    {
      POP = 1,
      UO,
      ZIP,
      ZOTZ,
      TZEC,
      XUL,
      YAXKIN,
      MOL,
      CHEN,
      YAX,
      ZAC,
      CEH,
      MAC,
      KANKIN,
      MUAN,
      PAX,
      KAYAB,
      CUMKU,
      UAYEB,
    };

    MONTH month; // 1-19
    int day; // 0-19

    [[nodiscard]] CAL_CONSTEXPR_FN int64_t ordinal() const {
      return (static_cast<int64_t>(month) - 1) * 20 + static_cast<int64_t>(day);
    }

    static constexpr int64_t epoch = LongDate::epoch - 348;

    CAL_DEF_OPS_CONSTEXPR(Haab)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Haab& o) const noexcept {
      if (month < o.month) return true;
      if (month != o.month) return false;
      return day < o.day;
    }

    [[nodiscard]] static CAL_CONSTEXPR_FN Haab from_date(const Date& date) noexcept {
      const auto count = math::mod(date.day - Haab::epoch, 365);
      const auto haabDay = math::mod(count, 20);
      const auto haabMonth = static_cast<Haab::MONTH>(math::floor(count / 20) + 1);
      return Haab{haabMonth, static_cast<int>(haabDay)};
    }
  };
}