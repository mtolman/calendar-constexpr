#pragma once

#include "../radix_date.h"
#include <array>

namespace calendars::mayan {
  /**
   * Represents the Mayan Long Date calendars system which does have absolute positioning in time
   * The Goodman-Martinez-Thompson correlation is used to determine how the long date corresponds
   *  to other calendaring systems. No attempt is made to support other correlations.
   */
  struct LongDate {
    static constexpr int64_t epoch = -1137142;
    using R = RadixDate<4, 0, 20, 20, 18, 20>;
    R value;

    CAL_DEF_OPS_CONSTEXPR(LongDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const LongDate& o) const noexcept { return value < o.value; }

    [[nodiscard]] static CAL_CONSTEXPR_FN LongDate from_date(const Date& date) noexcept {
      return LongDate{Date{date.day - LongDate::epoch}.to<LongDate::R>()};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Date to_date() const noexcept {
      return Date{LongDate::epoch + Date::from(value).day};
    }
  };
}