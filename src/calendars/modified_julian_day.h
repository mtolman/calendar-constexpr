#pragma once

#include "common.h"

namespace calendars {

  /**
   * Modified Julian Date number which has a closer Epoch and which starts it's days at midnight.
   * An ModifiedJulianDay is often a substitute for JdDates when tracking dates closer to the
   * present time as the closer Epoch lowers the amount of accuracy needed to track the time of day,
   * The ModifiedJulianDay tracks the number of days since November 17, 1858 C.E.
   *
   * MjdDates are often used for astronomy.
   */
  struct ModifiedJulianDay {
    /**
     * Number of days since November 17, 1858 C.E.
     */
    double                  datetime;
    static constexpr double epoch = 678576;

    CAL_CONSTEXPR_CLASS_FN ModifiedJulianDay() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN ModifiedJulianDay(double datetime) noexcept : datetime(datetime) {}

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const ModifiedJulianDay& o) const noexcept { return datetime < o.datetime; }

    CAL_DEF_OPS_CONSTEXPR(ModifiedJulianDay)

    [[nodiscard]] static CAL_CONSTEXPR_FN ModifiedJulianDay from_moment(const Moment& moment) noexcept {
      return ModifiedJulianDay{moment.datetime - ModifiedJulianDay::epoch};
    }

    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      return Moment{datetime + ModifiedJulianDay::epoch};
    }
  };
}
