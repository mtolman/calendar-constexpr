#pragma once

#include "common.h"
#include <numeric>
#include <algorithm>
#include <array>

namespace calendars {
  namespace impl {
    /**
     * Holds the conversion numbers for the radix date in arrays and holds the logic to convert to
     * and from moments.
     */
    template<int NumDayUnitsMinus1, int NumTimeUnits, int... UnitRatios>
    struct Radix {
      static constexpr int                  ARRAY_NUM_SIZE = NumDayUnitsMinus1 + NumTimeUnits + 1;
      std::array<double, NumDayUnitsMinus1> int_segment;
      std::array<double, NumTimeUnits>      frac_segment;

      /**
       * Static constexpr function to initialize a Radix from an initializer list of integer
       * values
       * @param values
       * @return
       */
      static constexpr Radix init_with(std::initializer_list<int> values) noexcept {
        auto        radix    = Radix{};
        const auto* iterator = values.begin();

        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        for (size_t i = 0; i < NumDayUnitsMinus1 && iterator != values.end(); ++i, ++iterator) {
          radix.int_segment.at(i) = *iterator;
        }

        // NOLINTNEXTLINE(cppcoreguidelines-pro-bounds-pointer-arithmetic)
        for (size_t f = 0; f < NumTimeUnits && iterator != values.end(); ++f, ++iterator) {
          radix.frac_segment.at(f) = *iterator;
        }
        return radix;
      }
    };
  }  // namespace impl

  /**
   * Represents date (and optionally datetimes) as an array of numbers with each number
   * representing a part of the date. When representing dates this way, conversions are done
   * assuming a perfectly cyclical number of units for each segment (non-cyclical segments, such
   * as Gregorian months, will not get converted to and from correctly). For example, a date could
   * be represented as [weeks, days, hours, minutes, seconds] where each week is 7 days, each day
   * is 24 hours, each hour 60 minutes, and each minute 60 seconds. This calendars would be
   * represented by RadixDate<1, 3, 7, 24, 60, 60>.
   *
   * The template parameters are split into three sections: Number of day units, number of time
   * units, and unit relationships. The number of day units is the total number of day-based
   * segments minus 1. The minus 1 is used since the largest unit term automatically added. So,
   * for dates with only time segments, a day count field will be automatically added. In
   * the example above of [weeks, days, hours, minutes, seconds], weeks is automatically added.
   *
   * The number of time units is used to represent portions of a day, or the time of day.
   * If the time of day is undesirable, set NUM_TIME_UNITS to 0.
   *
   * The unit relationships determines how many of the smaller units go into the larger units.
   * The smallest units are on the right and the largest are on the left.
   * The total number of unit sizes that need to be provided are NumDayUnitsMinus1 +
   * NumTimeUnits.
   *
   * @tparam NumDayUnitsMinus1 Number of day-sized units (minus 1)
   * @tparam NumTimeUnits Number of time-based units
   * @tparam UnitRatios List of unit ratios
   */
  template<int NumDayUnitsMinus1, int NumTimeUnits, int... UnitRatios>
  struct RadixDate {
    static_assert(!templ::utils::containsZero<UnitRatios...>::value, "Cannot have a 0 ratio");
    static_assert(!templ::utils::containsNegative<UnitRatios...>::value, "Cannot have a negative ratio");

   public:
    static constexpr int  NUM_DAY_UNITS_MINUS_1 = NumDayUnitsMinus1;
    static constexpr int  NUM_TIME_UNITS        = NumTimeUnits;
    static constexpr auto UNIT_RATIOS           = std::array{UnitRatios...};

    using Radix = impl::Radix<NumDayUnitsMinus1, NumTimeUnits, UnitRatios...>;

    static_assert((sizeof...(UnitRatios)) < Radix::ARRAY_NUM_SIZE, "Cannot have a negative ratio");

    /**
     * Radix base for converting to and from moments
     */
    static constexpr const Radix base = Radix::init_with({UnitRatios...});

    /**
     * Underlying value for the current radix date
     */
    std::array<double, Radix::ARRAY_NUM_SIZE> value;

    /**
     * Creates a RadixDate from an array of values
     * @param values
     */
    CAL_CONSTEXPR_CLASS_FN explicit RadixDate(const std::array<double, Radix::ARRAY_NUM_SIZE>& values) noexcept : value(values) {}

    RadixDate(const RadixDate& values) = default;
    RadixDate(RadixDate&& o) noexcept  = default;
    RadixDate& operator=(const RadixDate& other) = default;
    RadixDate& operator=(RadixDate&& other) noexcept = default;
    ~RadixDate()                                     = default;

    /**
     * Creates a RadixDate from an initializer list
     * @param values
     */
    CAL_CONSTEXPR_CLASS_FN RadixDate(const std::initializer_list<double>& args) noexcept {
      std::fill_n(value.begin(), Radix::ARRAY_NUM_SIZE - args.size(), 0);
      std::copy(args.begin(), args.end(), value.begin() + Radix::ARRAY_NUM_SIZE - args.size());
    }

    /**
     * Gets the associated element from the radix date. If the index is out of bounds, it will
     * throw an exception if out of bounds
     * @param index
     * @throws out_of_range
     * @return
     */
    double& operator[](size_t index) {
      if (index >= Radix::ARRAY_NUM_SIZE) {
        throw std::out_of_range("Index out of bounds");
      }
      else {
        return value[ index ];
      }
    }

    CAL_DEF_OPS_CONSTEXPR(RadixDate)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const RadixDate& o) const noexcept {
      for(size_t i = 0; i < value.size(); ++i) {
        if (value.at(i) >= o.value.at(i)) {
          return false;
        }
      }
      return true;
    }


    /**
     * Gets the associated element from the radix date. Does a compile time check to make sure the
     * Index is in-bounds
     * @tparam Index
     * @return
     */
    template<int Index>
    [[nodiscard]] CAL_CONSTEXPR_FN double& get() noexcept {
      static_assert(Index < Radix::ARRAY_NUM_SIZE, "Index out of bounds");
      return value[ Index ];
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN RadixDate from_moment(const Moment& moment) noexcept {
      RadixDate result{};
      
      double wholeCoefficient = 1.0;
      auto seg = base.int_segment;
      wholeCoefficient        = std::accumulate(seg.begin(), seg.end(), 1.0, std::multiplies<>());

      double fracCoefficient = 1.0;

      result.value[ 0 ] = math::floor(moment.datetime / wholeCoefficient);
      if (!base.int_segment.empty()) {
        wholeCoefficient /= base.int_segment.at(0);

        for (int i = 1; i <= NUM_DAY_UNITS_MINUS_1; ++i) {
          result.value.at(i) = math::mod(math::floor(moment.datetime / wholeCoefficient), base.int_segment.at(i - 1));
          if (i < NUM_DAY_UNITS_MINUS_1) {
            wholeCoefficient /= base.int_segment.at(i);
          }
        }
      }

      if (NUM_TIME_UNITS != 0) {
        for (int i = 0; i < NUM_TIME_UNITS - 1; ++i) {
          fracCoefficient *= base.frac_segment.at(i);
          result.value.at(i + NUM_DAY_UNITS_MINUS_1 + 1) = math::mod(math::floor(moment.datetime * fracCoefficient), base.frac_segment.at(i));
        }

        fracCoefficient *= base.frac_segment.at(NUM_TIME_UNITS - 1);
        result.value.at(NUM_DAY_UNITS_MINUS_1 + NUM_TIME_UNITS) =
            math::mod(moment.datetime * fracCoefficient, base.frac_segment.at(NUM_TIME_UNITS - 1));
      }
      return result;
    }
    
    [[nodiscard]] CAL_CONSTEXPR_FN Moment to_moment() const noexcept {
      double result = 0.0;
      
      double wholeCoefficient = std::accumulate(base.int_segment.begin(), base.int_segment.end(), 1.0, std::multiplies<>());
      double fracCoefficient  = 1.0;

      for (size_t i = 0; i <= NUM_DAY_UNITS_MINUS_1; ++i) {
        result += value.at(i) * wholeCoefficient;
        if (i < NUM_DAY_UNITS_MINUS_1) {
          wholeCoefficient /= base.int_segment.at(i);
        }
      }

      for (size_t i = 0; i < NUM_TIME_UNITS; ++i) {
        fracCoefficient *= base.frac_segment.at(i);
        result += value.at(NUM_DAY_UNITS_MINUS_1 + 1 + i) / fracCoefficient;
      }

      return Moment{result};
    }
  };
  
  
  namespace impl {
    template<typename T>
    struct IsRadixDate;

    template<int NumDayUnitsMinus1, int NumTimeUnits, int... UnitRatios>
    struct IsRadixDate<RadixDate<NumDayUnitsMinus1, NumTimeUnits, UnitRatios...>> {
      static constexpr bool value = true;
    };

    template<typename T>
    struct IsRadixDate {
      static constexpr bool value = false;
    };
  }  // namespace impl

  template<typename T>
  constexpr bool is_radix_date = impl::IsRadixDate<T>::value; // NOLINT(readability-identifier-naming)
}
