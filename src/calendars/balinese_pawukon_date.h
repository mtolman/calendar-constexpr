#pragma once

#include "common.h"
#include <array>

namespace calendars {

  /**
   * Represents a Balinese Pawukon calendars
   */
  struct BalinesePawukonDate {
    static constexpr int64_t epoch = -1721279;

    bool luang;
    enum class DWIWARA : int8_t {
      MENGA = 1, PEPET
    } dwiwara;
    enum class TRIWARA : int8_t {
      PASAH = 1, BETENG, KAJENG
    } triwara;
    enum class CATURWARA : int8_t {
      SRI = 1, LABA, JAYA, MENALA
    } caturwara;
    enum class PANCAWARA : int8_t {
      UMANIS = 1, PAING, PON, WAGE, KELIWON
    } pancawara;
    enum class SADWARA : int8_t {
      TUNGLEH = 1, ARYANG, URUKUNG, PANIRON, WAS, MAULU
    } sadwara;
    enum class SAPTAWARA : int8_t {
      REDITE = 1, COMA, ANGGARA, BUDA, WRASPATI, SUKRA, SANISCARA
    } saptawara;
    enum class ASATAWARA : int8_t {
      SRI = 1, INDRA, GURU, YAMA, LUDRA, BRAHMA, KALA, UMA
    } asatawara;
    enum class SANGAWARA : int8_t {
      DANGU = 1, JANGUR, GIGIS, NOHAN, OGAN, ERANGAN, URUNGAN, TULUS, DADI
    } sangawara;
    enum class DASAWARA : int8_t {
      PANDITA = 1, PATI, SUKA, DUKA, SRI, MANUH, MANUSA, RAJA, DEWA, RAKSASA = 0
    } dasawara;

    CAL_CONSTEXPR_CLASS_FN BalinesePawukonDate() = default;
    CAL_CONSTEXPR_CLASS_FN BalinesePawukonDate(bool luang, int dwiwara, int triwara, int caturwara, int pancawara,
                        int sadwara, int saptawara, int asatawara, int sangawara, int dasawara)
        : luang(luang), dwiwara(static_cast<DWIWARA>(dwiwara)), triwara(static_cast<TRIWARA>(triwara)),
        caturwara(static_cast<CATURWARA>(caturwara)), pancawara(static_cast<PANCAWARA>(pancawara)),
        sadwara(static_cast<SADWARA>(sadwara)), saptawara(static_cast<SAPTAWARA>(saptawara)),
        asatawara(static_cast<ASATAWARA>(asatawara)), sangawara(static_cast<SANGAWARA>(sangawara)),
        dasawara(static_cast<DASAWARA>(dasawara)) {}
    CAL_CONSTEXPR_CLASS_FN BalinesePawukonDate(bool luang, DWIWARA dwiwara, TRIWARA triwara, CATURWARA caturwara,
                        PANCAWARA pancawara, SADWARA sadwara, SAPTAWARA saptawara,
                        ASATAWARA asatawara, SANGAWARA sangawara, DASAWARA dasawara)
        : luang(luang), dwiwara(dwiwara), triwara(triwara), caturwara(caturwara),
          pancawara(pancawara), sadwara(sadwara), saptawara(saptawara),
          asatawara(asatawara), sangawara(sangawara), dasawara(dasawara) {}


    [[nodiscard]] CAL_CONSTEXPR_FN bool operator==(const BalinesePawukonDate& o) const noexcept {
      return luang == o.luang && dwiwara == o.dwiwara && triwara == o.triwara && caturwara == o.caturwara &&
        pancawara == o.pancawara && sadwara == o.sadwara && saptawara == o.saptawara && asatawara == o.asatawara &&
        sangawara == o.sangawara && dasawara == o.dasawara;
    }
    [[nodiscard]] CAL_CONSTEXPR_FN bool operator!=(const BalinesePawukonDate& o) const noexcept { return !(*this == o); }

    static CAL_CONSTEXPR_FN BalinesePawukonDate from_date(const Date& date) {
      return BalinesePawukonDate{
            bali_luang_from_fixed(date),
            bali_dwiwara_from_fixed(date),
            bali_triwara_from_date(date),
            bali_caturwara_from_date(date),
            bali_pancawara_from_date(date),
            bali_sadwara_from_date(date),
            bali_saptawara_from_date(date),
            bali_astawara_from_date(date),
            bali_sangawara_from_fixed(date),
            bali_dasawara_from_date(date)
      };
    }

    static CAL_CONSTEXPR_FN int32_t bali_day_from_date(const Date& date) noexcept {
      return math::mod<int64_t, int32_t>(date.day - epoch, 210);
    }

    static CAL_CONSTEXPR_FN TRIWARA bali_triwara_from_date(const Date& date) noexcept {
      return static_cast<TRIWARA>(math::mod(bali_day_from_date(date), 3) + 1);
    }

    static CAL_CONSTEXPR_FN SADWARA bali_sadwara_from_date(const Date& date) noexcept {
      return static_cast<SADWARA>(math::mod(bali_day_from_date(date), 6) + 1);
    }

    static CAL_CONSTEXPR_FN SAPTAWARA bali_saptawara_from_date(const Date& date) noexcept {
      return static_cast<SAPTAWARA>(math::mod(bali_day_from_date(date), 7) + 1);
    }

    static CAL_CONSTEXPR_FN PANCAWARA bali_pancawara_from_date(const Date& date) noexcept {
      return static_cast<PANCAWARA>(math::amod(bali_day_from_date(date) + 2, 5));
    }

    static CAL_CONSTEXPR_FN int32_t bali_week_from_date(const Date& date) noexcept {
      return math::floor<double, int32_t>(static_cast<double>(bali_day_from_date(date)) / 6.0) + 1;
    }

    static constexpr auto dasawara_i_arr = std::array { 5, 9, 7, 4, 8 };
    static constexpr auto dasawara_j_arr = std::array { 5, 4, 3, 7, 8, 6, 9 };
    static CAL_CONSTEXPR_FN DASAWARA bali_dasawara_from_date(const Date& date) noexcept {
      const auto i = static_cast<int>(bali_pancawara_from_date(date)) - 1;
      const auto j = static_cast<int>(bali_saptawara_from_date(date)) - 1;
      return static_cast<DASAWARA>(math::mod(1 + dasawara_i_arr.at(i) + dasawara_j_arr.at(j), 10));
    }
    
    static CAL_CONSTEXPR_FN DWIWARA bali_dwiwara_from_fixed(const Date& date) noexcept {
      return static_cast<DWIWARA>(math::amod(static_cast<int>(bali_dasawara_from_date(date)), 2));
    }

    static CAL_CONSTEXPR_FN bool bali_luang_from_fixed(const Date& date) noexcept {
      return math::mod(static_cast<int>(bali_dasawara_from_date(date)), 2) == 0;
    }
    
    static CAL_CONSTEXPR_FN SANGAWARA bali_sangawara_from_fixed(const Date& date) noexcept {
      return static_cast<SANGAWARA>(math::mod(math::max(0, bali_day_from_date(date) - 3), 9) + 1);
    }

    static CAL_CONSTEXPR_FN ASATAWARA bali_astawara_from_date(const Date& date) noexcept {
      return static_cast<ASATAWARA>(math::mod(math::max(6, 4 + math::mod(bali_day_from_date(date) - 70, 210)), 8) + 1);
    }

    static CAL_CONSTEXPR_FN CATURWARA bali_caturwara_from_date(const Date& date) noexcept {
      return static_cast<CATURWARA>(math::amod(static_cast<int>(bali_astawara_from_date(date)), 4));
    }
  };

  CAL_CONSTEXPR_FN Date bali_on_or_before(const BalinesePawukonDate& bDate, const Date& date) noexcept {
    const auto a5 = static_cast<int>(bDate.pancawara) - 1;
    const auto a6 = static_cast<int>(bDate.sadwara) - 1;
    const auto b7 = static_cast<int>(bDate.saptawara) - 1;
    const auto b35 = math::mod(a5 + 14 + 15 * (b7 - a5), 35);
    const auto days = a6 + 36 * (b35 - a6);
    const auto delta = BalinesePawukonDate::bali_day_from_date(Date{0});
    return Date{date.day - math::mod(date.day + delta - days, 210)};
  }
}
