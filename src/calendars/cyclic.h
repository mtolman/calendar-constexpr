#pragma once

#include "common.h"

namespace calendars {
  enum class SingleCycleType {
    STRICTLY_BEFORE,
    UP_TO_AND_INCLUDING,
    MEAN_MONTH,
    AT_OR_BEFORE_MEAN_MONTH
  };

  template<
      SingleCycleType TYPE,
      int64_t EPOCH,
      int64_t YEAR_NUMERATOR,
      int64_t MONTH_NUMERATOR,
      int64_t YEAR_DENOMINATOR = 1,
      int32_t MONTH_DENOMINATOR = 1,
      int32_t YEAR_OFFSET_NUMERATOR = 0,
      int32_t YEAR_OFFSET_DENOMINATOR = 1,
      int32_t MONTH_OFFSET_NUMERATOR = 0,
      int32_t MONTH_OFFSET_DENOMINATOR = 1>
  struct SingleCycle {
    static constexpr int64_t epoch = EPOCH;
    static constexpr double Y = static_cast<double>(YEAR_NUMERATOR) / static_cast<double>(YEAR_DENOMINATOR);
    static constexpr double M = static_cast<double>(MONTH_NUMERATOR) / static_cast<double>(MONTH_DENOMINATOR);
    static constexpr double deltaY = static_cast<double>(YEAR_OFFSET_NUMERATOR) / static_cast<double>(YEAR_OFFSET_DENOMINATOR);
    static constexpr double deltaM = static_cast<double>(MONTH_OFFSET_NUMERATOR) / static_cast<double>(MONTH_OFFSET_DENOMINATOR);
    static constexpr SingleCycleType type = TYPE;
    static_assert(
        (TYPE != SingleCycleType::MEAN_MONTH && TYPE != SingleCycleType::AT_OR_BEFORE_MEAN_MONTH)
            || (static_cast<double>(static_cast<int64_t>(Y/M)) == Y/M), "Mean Month Restriction: Must be an integer number of months per year");
    static_assert(
        (TYPE != SingleCycleType::MEAN_MONTH && TYPE != SingleCycleType::AT_OR_BEFORE_MEAN_MONTH)
            || (deltaM == 0), "Mean Month Restriction: There cannot be a month offset");

    int64_t year;
    int32_t month;
    int32_t day;

    CAL_CONSTEXPR_FN Date to_date() const noexcept {
      if constexpr (type == SingleCycleType::STRICTLY_BEFORE) {
        return Date{epoch + math::floor<double, int64_t>((year - 1) * Y + deltaY) + math::floor<double, int64_t>((month - 1) * M + deltaM) + day - 1};
      }
      else if constexpr (type == SingleCycleType::UP_TO_AND_INCLUDING) {
        return Date{epoch + math::ceil<double, int64_t>((year - 1) * Y - deltaY)
            + math::floor((month - 1) * M  + deltaM) + day - 1};
      }
      else if constexpr (type == SingleCycleType::MEAN_MONTH) {
        return Date{
          epoch + math::floor<double, int64_t>((year - 1) * Y + deltaY + (month - 1) * M)
              + day - 1
        };
      }
      else if constexpr (type == SingleCycleType::AT_OR_BEFORE_MEAN_MONTH) {
        return Date{
          epoch + math::ceil<double, int64_t>((year - 1) * Y + deltaY + (month - 1) * M)
              + day - 1
        };
      }
    }

    static CAL_CONSTEXPR_FN SingleCycle from_date(const Date& date) noexcept {
      if constexpr (type == SingleCycleType::STRICTLY_BEFORE) {
        const auto d     = static_cast<double>(date.day - epoch + 1) - deltaY;
        const auto year  = math::ceil<double, int64_t>(d / Y);
        const auto n     = math::ceil<double, double>(d - (year - 1) * Y) - deltaM;
        const auto month = math::ceil<double, int32_t>(n / M);
        const auto day   = math::ceil<double, int32_t>(math::amod(n, M));
        return {year, month, day};
      }
      else if constexpr (type == SingleCycleType::UP_TO_AND_INCLUDING) {
        const auto d     = static_cast<double>(date.day - epoch) + deltaY;
        const auto year  = math::floor<double, int64_t>(d/Y) + 1;
        const auto n     = math::floor(math::mod(d, Y)) + 1 - deltaM;
        const auto month = math::ceil<double, int32_t>(n / M);
        const auto day   = math::ceil<double, int32_t>(math::amod(n, M));
        return {year, month, day};
      }
      else if constexpr (type == SingleCycleType::MEAN_MONTH) {
        const auto d = static_cast<double>(date.day - epoch + 1) - deltaY;
        const auto year = math::ceil<double, int64_t>(d / Y);
        const int32_t L = Y/M;
        const auto m = math::ceil(d/M) - 1;
        const int32_t month = math::mod(m, L + 1);
        const auto day = math::ceil<double, int32_t>(math::amod(d, M));
        return {year, month, day};
      }
      else if constexpr (type == SingleCycleType::AT_OR_BEFORE_MEAN_MONTH) {
        const auto d = static_cast<double>(date.day - epoch) - deltaY;
        const auto year = math::floor<double, int64_t>(d / Y) + 1;
        const int32_t L = Y/M;
        const int32_t month = math::mod(math::floor<double, int32_t>(d/M), L + 1);
        const auto day = math::floor<double, int32_t>(math::mod(d, M)) + 1;
        return {year, month, day};
      }
    }

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const SingleCycle& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(SingleCycle)
  };

  enum class DoubleCycleType {
    STRICTLY_BEFORE,
    AT_OR_BEFORE,
  };

  template<
      DoubleCycleType TYPE,
      int64_t EPOCH,
      int64_t YEAR_NUMERATOR,
      int64_t MONTH_NUMERATOR,
      int64_t YEAR_DENOMINATOR = 1,
      int32_t MONTH_DENOMINATOR = 1,
      int32_t YEAR_OFFSET_NUMERATOR = 0,
      int32_t YEAR_OFFSET_DENOMINATOR = 1,
      int32_t MONTH_OFFSET_NUMERATOR = 0,
      int32_t MONTH_OFFSET_DENOMINATOR = 1
  >
  struct DoubleCycle {
    static constexpr int64_t epoch = EPOCH;
    static constexpr DoubleCycleType type = TYPE;
    static constexpr double Y = static_cast<double>(YEAR_NUMERATOR) / static_cast<double>(YEAR_DENOMINATOR);
    static constexpr double M = static_cast<double>(MONTH_NUMERATOR) / static_cast<double>(MONTH_DENOMINATOR);
    static constexpr double deltaY = static_cast<double>(YEAR_OFFSET_NUMERATOR) / static_cast<double>(YEAR_OFFSET_DENOMINATOR);
    static constexpr double deltaM = static_cast<double>(MONTH_OFFSET_NUMERATOR) / static_cast<double>(MONTH_OFFSET_DENOMINATOR);
    static constexpr double leapYearFrac = math::mod(static_cast<double>(Y) / static_cast<double>(M), 1);
    static_assert(
        (static_cast<double>(static_cast<int64_t>(Y/M)) == Y/M),
        "Must be an integer number of months per year"
    );

    static constexpr int32_t L = Y/M;

    int64_t year;
    int32_t month;
    int32_t day;

    CAL_CONSTEXPR_FN Date to_date() const noexcept {
      if constexpr (type == DoubleCycleType::STRICTLY_BEFORE) {
        const auto m = math::floor<double, int64_t>((year-1) * L + deltaY) + month - 1;
        return Date{epoch + math::floor<double, int64_t>(m*M+deltaM) + day - 1};
      }
      else if constexpr (type == DoubleCycleType::AT_OR_BEFORE) {
        const auto m = math::floor<double, int64_t>((year - 1) * L + deltaY) + month - 1;
        return Date{epoch + math::ceil(m*M - deltaM) + day - 1};
      }
    }

    static CAL_CONSTEXPR_FN DoubleCycle from_date(const Date& date) noexcept {
      if constexpr (type == DoubleCycleType::STRICTLY_BEFORE) {
        const auto d = date.day - epoch + 1 - deltaM;
        const auto m = math::ceil(d*L) - deltaY;
        const auto year = math::ceil<double, int64_t>(m/L);
        const auto month = math::ceil<double, int32_t>(math::amod(m, L));
        const auto day = math::ceil<double, int32_t>(math::amod(d, M));
        return {year, month, day};
      }
      else if constexpr (type == DoubleCycleType::AT_OR_BEFORE) {
        const auto d = date.day - epoch + deltaM;
        const auto m = math::ceil(d/M) + 1 - deltaY;
        const auto year = math::ceil<double, int64_t>(m/L);
        const auto month = math::ceil<double, int32_t>(math::amod(m, L));
        const auto day = math::floor<double, int32_t>(math::mod(d, M)) + 1;
        return {year, month, day};
      }
    }

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const DoubleCycle& o) const noexcept {
      return chain_lt(year, o.year, month, o.month, day, o.day);
    }

    CAL_DEF_OPS_CONSTEXPR(DoubleCycle)
  };
}