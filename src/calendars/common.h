#pragma once

#include <cinttypes>
#include <vector>
#include "../common.h"
#include "../template_utils.h"
#include "../cal_math.h"

/**
 * Root namespace for all calendars
 */
namespace calendars {
  struct Moment;
  struct Date;

  /**
   * Enum class for each day of the week.
   * Days of week start with Sunday and run through Saturday.
   * Additionally, the days of the week are a 0-based index.
   * This means that SUNDAY = 0, MONDAY = 1, ...
   */
  enum class DAY_OF_WEEK : int8_t {
    SUNDAY    = 0,
    MONDAY    = 1,
    TUESDAY   = 2,
    WEDNESDAY = 3,
    THURSDAY  = 4,
    FRIDAY    = 5,
    SATURDAY  = 6
  };

  namespace impl {
    template<typename U>
    class HasFromMoment {
     private:
      template<typename T, T>
      struct helper;
      template<typename T>
      static std::uint8_t check(helper<T (*)(const Moment&), &T::from_moment>*);
      template<typename T>
      static std::uint16_t check(...);
     public:
      static constexpr bool value = sizeof(check<U>(0)) == sizeof(std::uint8_t);
    };

    template<typename U>
    class HasFromDate {
     private:
      template<typename T, T>
      struct helper;
      template<typename T>
      static std::uint8_t check(helper<T (*)(const Date&), &T::from_date>*);
      template<typename T>
      static std::uint16_t check(...);
     public:
      static constexpr bool value = sizeof(check<U>(0)) == sizeof(std::uint8_t);
    };

    template<typename, typename T>
    struct HasToMomentImpl {
      static_assert(
          std::integral_constant<T, false>::value,
          "Second template parameter needs to be of function type.");
    };
    template<typename C, typename Ret, typename... Args>
    struct HasToMomentImpl<C, Ret(Args...)> {
     private:
      template<typename T>
      static constexpr auto check(T*)
          -> typename
          std::is_same<
              decltype( std::declval<T>().to_moment( std::declval<Args>()... ) ),
              Ret
              >::type;

      template<typename>
      static constexpr std::false_type check(...);

      typedef decltype(check<C>(0)) type;

     public:
      static constexpr bool value = type::value;
    };

    template<typename, typename T>
    struct HasToDateImpl {
      static_assert(
          std::integral_constant<T, false>::value,
          "Second template parameter needs to be of function type.");
    };
    template<typename C, typename Ret, typename... Args>
    struct HasToDateImpl<C, Ret(Args...)> {
     private:
      template<typename T>
      static constexpr auto check(T*)
          -> typename
          std::is_same<
              decltype( std::declval<T>().to_date( std::declval<Args>()... ) ),
              Ret
              >::type;

      template<typename>
      static constexpr std::false_type check(...);

      typedef decltype(check<C>(0)) type;

     public:
      static constexpr bool value = type::value;
    };
  }

  template<typename T>
  constexpr bool has_to_moment = impl::HasToMomentImpl<T, Moment(void)>::value; // NOLINT(readability-identifier-naming)

  template<typename T>
  constexpr bool has_from_moment = impl::HasFromMoment<T>::value; // NOLINT(readability-identifier-naming)

  template<typename T>
  constexpr bool has_moment_bidir_convert = std::is_same_v<T, Moment> || (has_to_moment<T> && has_from_moment<T>); // NOLINT(readability-identifier-naming)

  template<typename T>
  constexpr bool has_to_date = impl::HasToDateImpl<T, Date(void)>::value; // NOLINT(readability-identifier-naming)

  template<typename T>
  constexpr bool has_from_date = impl::HasFromDate<T>::value; // NOLINT(readability-identifier-naming)

  template<typename T>
  constexpr bool has_date_bidir_convert = std::is_same_v<T, Date> || has_moment_bidir_convert<T> || (has_to_date<T> && has_from_date<T>); // NOLINT(readability-identifier-naming)

#define CAL_DEF_OPS_CONSTEXPR(TYPE)                                                                                                                  \
  [[nodiscard]] CAL_CONSTEXPR_FN bool operator>=(const TYPE& o) const noexcept { return !(*this < o); }                                                            \
  [[nodiscard]] CAL_CONSTEXPR_FN bool operator>(const TYPE& o) const noexcept { return (o < *this); }                                                              \
  [[nodiscard]] CAL_CONSTEXPR_FN bool operator<=(const TYPE& o) const noexcept { return !(o < *this); }                                                            \
  [[nodiscard]] CAL_CONSTEXPR_FN bool operator==(const TYPE& o) const noexcept { return !(*this < o || *this > o); }                                               \
  [[nodiscard]] CAL_CONSTEXPR_FN bool operator!=(const TYPE& o) const noexcept { return !(*this == o); }



  /**
   * Represents the total time from the Date epoch.
   * Unlike Date, it will avoid rounding whenever possible.
   */
  struct Moment {
    static constexpr double epoch = 0;
    /**
     * Time passed since the Date epoch.
     * 1.0 represents midnight on January 1, 1 C.E.
     */
    double datetime;

    CAL_CONSTEXPR_CLASS_FN Moment() noexcept = default;
    CAL_CONSTEXPR_CLASS_FN explicit Moment(double datetime) noexcept : datetime(datetime) {}

    /**
     * Returns a double representing the time component of the moment
     * @return A double between [0, 1)
     */
    [[nodiscard]] CAL_CONSTEXPR_FN double to_time() const noexcept { return math::mod(datetime, 1.0); }

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Moment& o) const noexcept { return datetime < o.datetime; }

    CAL_DEF_OPS_CONSTEXPR(Moment)

    /**
     * Handles converting a Moment to a target date
     * @tparam T
     * @return
     */
    template<typename T>
    [[nodiscard]] CAL_CONSTEXPR_CLASS_FN T to() const noexcept {
      static_assert(has_from_moment<T> || std::is_same_v<T, Moment>, "Type T must implement static method 'T from_moment(const Moment&) noexcept'");
      if constexpr (std::is_same_v<T, Moment>) {
        return *this;
      }
      else {
        return T::from_moment(*this);
      }
    }

    /**
     * Handles converting a Moment to a target date
     * @tparam T
     * @return
     */
    template<typename T>
    [[nodiscard]] static CAL_CONSTEXPR_CLASS_FN Moment from(const T& date) noexcept {
      static_assert(has_to_moment<T> || std::is_same_v<std::decay_t<T>, Moment>, "Type T must implement member method 'Moment to_moment() const noexcept'");
      if constexpr (std::is_same_v<std::decay_t<T>, Moment>) {
        return date;
      }
      else {
        return date.to_moment();
      }
    }
  };


  /**
   * Date
   * This is the abstract, common representation of any date which counts the number of days since
   * it's epoch, with 1 r.d. being equal to Monday January 1, 1 C.E. (Gregorian).
   *
   * Note that RD Dates do not represent the time of day, only the number of days that have passed.
   * As such, no timezone or time information is stored on an Date.
   *
   * If time is needed, prefer a Moment.
   */
  struct Date {
    /**
     * The number of days since the epoch
     */
    int64_t day = 0;

    static constexpr int64_t epoch = 0;

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Date& o) const noexcept { return day < o.day; }

    CAL_DEF_OPS_CONSTEXPR(Date)

    CAL_CONSTEXPR_CLASS_FN Date(const Date& d) noexcept = default;
    ~Date()                                               = default;
    CAL_CONSTEXPR_CLASS_FN Date(Date&& d) noexcept      = default;

    Date& operator=(const Date& d) = default;
    Date& operator=(Date&& d) noexcept = default;

    /**
     * Constructs a new Date from a double representing the number of days elapsed with 1 being
     * January 1, 1 C.E. (Gregorian)
     * @param d
     */
    template<class T>
    CAL_CONSTEXPR_CLASS_FN explicit Date(T d) noexcept {
      if constexpr (std::is_same<typename std::decay<T>::type, Date>::value) {
        day = d.day;
      }
      else if constexpr (std::is_floating_point<T>::value) {
        day = math::round<T, int64_t>(d);
      }
      else if constexpr (std::is_integral<T>::value) {  // NOLINT(bugprone-branch-clone)
        day = static_cast<int64_t>(d);
      }
      else if constexpr (std::is_same<typename std::decay<T>::type, Date*>::value) {
        day = d->day;
      }
      else if constexpr (std::is_convertible_v<T, double>) {
        day = math::round<double, int64_t>(static_cast<double>(d));
      }
      else if constexpr (std::is_convertible_v<T, float>) {
        day = math::round<float, int64_t>(static_cast<float>(d));
      }
      else if constexpr (std::is_convertible_v<T, int64_t>) {
        day = static_cast<int64_t>(d);
      }
      else if constexpr (std::is_convertible_v<T, int32_t>) {
        day = static_cast<int32_t>(d);
      }
      else if constexpr (std::is_convertible<T, Date>::value) {
        day = static_cast<Date>(d).day;
      }
      else {
        day = Date::from(d).day;
      }
    }

    /**
     * Creates a new Date with days set to the epoch
     */
    CAL_CONSTEXPR_CLASS_FN Date() noexcept = default;

    template<class T>
    [[nodiscard]] static CAL_CONSTEXPR_CLASS_FN Date from(const T& date) noexcept {
      static_assert(has_to_date<T> || std::is_same_v<std::decay_t<T>, Date> || std::is_same_v<std::decay_t<T>, Moment> || has_to_moment<T>, "Type T must provide the member method 'Date to_date() const noexcept'");
      if constexpr (std::is_same_v<std::decay_t<T>, Date>) {
        return date;
      }
      else if constexpr (std::is_same_v<std::decay_t<T>, Moment>) {
        return Date{math::floor<decltype(Moment::datetime), int64_t>(date.datetime)};
      }
      else if constexpr (has_to_date<T>) {
        return date.to_date();
      }
      else {
        return from(Moment::from(date));
      }
    }

    template<class T>
    [[nodiscard]] CAL_CONSTEXPR_CLASS_FN T to() const noexcept {
      static_assert(has_from_date<T> || std::is_same_v<T, Date> || std::is_same_v<T, Moment> || has_from_moment<T>, "Type T must provide the static method 'T from_date(const Date&) noexcept'");
      if constexpr (std::is_same_v<std::decay_t<T>, Date>) {
        return *this;
      }
      else if constexpr (std::is_same_v<T, Moment>) {
        return Moment{static_cast<decltype(Moment::datetime)>(day)};
      }
      else if constexpr(has_from_date<T>) {
        return T::from_date(*this);
      }
      else {
        return to<Moment>().to<T>();
      }
    }


    /**
     * Adds d days to the current Date
     *
     * @param d Number of days to add
     * @return New Date with the change
     */
    template<typename T>
    [[nodiscard]] CAL_CONSTEXPR_FN Date add_days(T d) const noexcept {
      return Date{day + static_cast<int64_t>(d)};
    }

    /**
     * Subtracts n days from the current Date
     *
     * @param n Number of days to subtract
     * @return New Date with the change
     */
    template<typename T>
    [[nodiscard]] CAL_CONSTEXPR_FN Date sub_days(T d) const noexcept {
      return Date{day - static_cast<int64_t>(d)};
    }

    /**
     * Returns the number of days between two Dates
     * @param other
     * @return
     */
    [[nodiscard]] CAL_CONSTEXPR_FN int64_t day_difference(const Date& other) const noexcept { return day - other.day; }

    /**
     * Gets the day of the week represented by the current date
     * @return Day of the week for the current date
     */
    [[nodiscard]] CAL_CONSTEXPR_CLASS_FN DAY_OF_WEEK day_of_week() const noexcept {
      auto zero = Date{};
      return static_cast<DAY_OF_WEEK>(calendars::math::mod(day - Date{&zero}.day - static_cast<int>(DAY_OF_WEEK::SUNDAY), 7.0));
    }

    /**
     * Gets the day of an m-cycle represented by the current date
     * The number returned will be from [0,m)
     *
     * @param m The number of days in the cycle (e.g. for day of the week, set m to 7)
     * @param offset The offset of the cycle (day of the week has an offset of 0)
     * @return An integer from [0,m) representing the day in the m-cycle
     */
    [[nodiscard]] CAL_CONSTEXPR_FN int32_t day_of_m_cycle(int32_t m, int32_t offset = 0) const noexcept {
#if RD_DATE_EPOCH != 0
      return static_cast<int32_t>(calendars::math::mod(day + Date::epoch - offset, m));
#else
      return static_cast<int32_t>(calendars::math::mod(day - offset, m));
#endif
    }

    //// (kday-on-or-before)
    /**
     * Finds the first date on or before the current date that occurs on the target day of the week
     * For example, if the current date was Date{1}, then
     * day_of_week_on_or_before(DAY_OF_WEEK::SUNDAY) would return Date{0}. If the current date was
     * Date{8}, then day_of_week_on_or_before(DAY_OF_WEEK::MONDAY) would return Date{8}
     *
     * @param d Day of the week to look for
     * @return An Date representing the day of the week
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date day_of_week_on_or_before(DAY_OF_WEEK d) const noexcept {
      return sub_days(static_cast<int32_t>((sub_days(static_cast<int32_t>(d))).day_of_week()));
    }

    //// (kday-on-or-after)
    /**
     * Finds the day of the week that occurs on or after the current date
     * For example, if the current date was Date{1}, then
     * day_of_week_on_or_after(DAY_OF_WEEK::SUNDAY) would return Date{7} If the current date was
     * Date{1}, then day_of_week_on_or_after(DAY_OF_WEEK::MONDAY) would return Date{1}
     *
     * @param d Day of the week to look for
     * @return An Date representing the day of the week
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date day_of_week_on_or_after(DAY_OF_WEEK d) const noexcept { return add_days(6).day_of_week_on_or_before(d); }

    //// (kday-on-or-nearest)
    /**
     * Finds the day of the week that occurs nearest to the current date
     * For example, if the current date was Date{1}, then day_of_week_nearest(DAY_OF_WEEK::SUNDAY)
     * would return Date{0}. However, if the current date was Date{6}, then
     * day_of_week_nearest(DAY_OF_WEEK::SUNDAY) would return Date{7}
     *
     * @param d Day of the week to look for
     * @return An Date representing the day of the week
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date day_of_week_nearest(DAY_OF_WEEK d) const noexcept { return add_days(3).day_of_week_on_or_before(d); }

    //// (kday-before)
    /**
     * Finds the day of the week that occurs before the current date
     * For example, if the current date was Date{8}, then day_of_week_nearest(DAY_OF_WEEK::MONDAY)
     * would return Date{1}.
     *
     * @param d Day of the week to look for
     * @return An Date representing the day of the week
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date day_of_week_before(DAY_OF_WEEK d) const noexcept { return sub_days(1).day_of_week_on_or_before(d); }

    //// (kday-after)
    /**
     * Finds the day of the week that occurs after the current date
     * For example, if the current date was Date{8}, then day_of_week_nearest(DAY_OF_WEEK::MONDAY)
     * would return Date{1}. If the current date was Date{1}, then
     * day_of_week_on_or_after(DAY_OF_WEEK::MONDAY) would return Date{8}.
     *
     * @param d Day of the week to look for
     * @return An Date representing the day of the week
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date day_of_week_after(DAY_OF_WEEK d) const noexcept { return add_days(7).day_of_week_on_or_before(d); }

    /**
     * Finds the kth day of the m-cycle that occurs on or before the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date kth_day_of_m_cycle_on_or_before(int32_t k, int32_t m, int32_t offset) const noexcept {
      return sub_days(sub_days(k).day_of_m_cycle(m, offset));
    }

    /**
     * Finds the kth day of the m-cycle that occurs before the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date kth_day_of_m_cycle_before(int32_t k, int32_t m, int32_t offset) const noexcept {
      return sub_days(1).kth_day_of_m_cycle_on_or_before(k, m, offset);
    }

    /**
     * Finds the kth day of the m-cycle that occurs after the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date kth_day_of_m_cycle_after(int32_t k, int32_t m, int32_t offset) const noexcept {
      return add_days(m).kth_day_of_m_cycle_on_or_before(k, m, offset);
    }

    /**
     * Finds the kth day of the m-cycle that occurs on or after the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date kth_day_of_m_cycle_on_or_after(int32_t k, int32_t m, int32_t offset) const noexcept {
      return add_days(m - 1).kth_day_of_m_cycle_on_or_before(k, m, offset);
    }

    /**
     * Finds the kth day of the m-cycle that occurs nearest the current date
     * @param k The kth day of the m-cycle
     * @param m The number of days in the m-cycle
     * @param offset The offset of the m-cycle
     * @return An Date with the result
     */
    [[nodiscard]] CAL_CONSTEXPR_FN Date kth_day_of_m_cycle_nearest_to(int32_t k, int32_t m, int32_t offset) const noexcept {
      return Date{static_cast<double>(this->day) + math::floor(static_cast<double>(m) / 2)}.kth_day_of_m_cycle_on_or_before(k, m, offset);
    }

    /**
     * Returns the nth occurrence of a day on a given week before the current date (or if n is
     * negative after d) If n is zero, it will return the current date instead
     * @param n The number of the occurrence before the current date (or if negative, the number of
     * the occurrence after)
     * @param k The week day to search for
     * @return
     */
    [[nodiscard]] CAL_CONSTEXPR_CLASS_FN Date nth_week_day(int n, DAY_OF_WEEK k) const noexcept {
      if (n > 0) {
        return day_of_week_before(k).add_days(7 * n);
      }
      else if (n < 0) {
        return day_of_week_after(k).add_days(7 * n);
      }
      else {
        return *this;
      }
    }

    [[nodiscard]] static std::vector<calendars::Date> positions_in_range(int64_t pth_moment, int64_t c_day_cycle, int64_t delta, Date start, const Date& end);

  };

  namespace impl {
#if CAL_USE_CONSTEXPR
    template<size_t... I>
    decltype(auto) n_dates_starting_at(const Date& start, std::index_sequence<I...> /* unused */) noexcept {
      return std::array<Date, sizeof...(I)>{start.add_days(static_cast<int64_t>(I))...};
    }
#else
    template<size_t... I>
    decltype(auto) n_dates_starting_at(const Date& start, size_t daysInYear) noexcept {
      std::vector<Date> res;
      res.reserve(daysInYear);
      for (size_t i = 0; i < daysInYear; ++i) {
        res.template emplace_back<>(start.template add_days<>(i));
      }
      return res;
    }
#endif
  }  // namespace impl

  template<typename T>
  CAL_CONSTEXPR_FN bool is_valid(const T& date) {
    static_assert(has_date_bidir_convert<std::decay_t<T>>, "Can only run is_valid on dates that have a bidirectional conversion with Date.");
    // Valid dates can convert to and from Date without changing
    return Date::from(date).template to<std::decay_t<T>>() == date;
  }

  namespace impl {
    CAL_CONSTEXPR_FN bool chain_lt() {
      return false;
    }

    template<typename L, typename R, typename ...T>
    CAL_CONSTEXPR_FN bool chain_lt(L left, R right, T... args) {
      return left < right || (left == right && chain_lt(args...));
    }
  }

  template<typename ...T>
  CAL_CONSTEXPR_FN bool chain_lt(T... args) {
    return impl::chain_lt(args...);
  }
}