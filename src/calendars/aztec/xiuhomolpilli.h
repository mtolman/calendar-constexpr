#pragma once

#include "tonalpohualli.h"
#include "xihuitl.h"
#include <optional>

namespace calendars::aztec {
  /**
   * Represents an Aztec Xiuhomolipilli date.
   * Note: must check if is valid before use
   */
  struct Xiuhomolpilli {
    enum class NAME : int16_t {
      CALLI = 3,
      TOCHTLI = 8,
      ACATL = 13,
      TECPATL = 18
    };

    int16_t number;
    NAME name;

    [[nodiscard]] CAL_CONSTEXPR_FN bool is_valid() const noexcept {
      switch (name) {
        case NAME::CALLI:
        case NAME::TOCHTLI:
        case NAME::ACATL:
        case NAME::TECPATL:
          return true;
        default:
          return false;
      }
    }

    CAL_DEF_OPS_CONSTEXPR(Xiuhomolpilli)

    [[nodiscard]] CAL_CONSTEXPR_FN bool operator<(const Xiuhomolpilli& o) const noexcept {
      if (number < o.number) return true;
      if (number != o.number) return false;
      return name < o.name;
    }
    
    [[nodiscard]] static CAL_CONSTEXPR_FN std::optional<Xiuhomolpilli> from_date(const Date& date) noexcept {
      const auto month = date.to<Xihuitl>().month;
      if (month == static_cast<Xihuitl::MONTH>(19)) {
        return std::nullopt;
      }
      const auto tonalpohualli = xihuitl_on_or_before(Date{date.day + 364}, Xihuitl{static_cast<Xihuitl::MONTH>(18), 20}).to<Tonalpohualli>();
      const auto res = Xiuhomolpilli{tonalpohualli.number, static_cast<Xiuhomolpilli::NAME>(tonalpohualli.name)};
      if (!res.is_valid()) {
        return std::nullopt;
      }
      return res;
    }
  };
}
