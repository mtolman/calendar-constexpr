#pragma once

#include "xihuitl.h"
#include "tonalpohualli.h"
#include <optional>

namespace calendars::aztec {

  [[nodiscard]] CAL_CONSTEXPR_FN Date tonalpohualli_on_or_before(const Date& date, const Tonalpohualli& tonalpohualli) noexcept {
    return Date{math::mod_range(Tonalpohualli::correlation + tonalpohualli.ordinal(), date.day, date.day - 260)};
  }

  [[nodiscard]] CAL_CONSTEXPR_FN Date xihuitl_on_or_before(const Date& date, const Xihuitl& xihuitl) noexcept {
    return Date{math::mod_range(Xihuitl::correlation + xihuitl.ordinal(), date.day, date.day - 365)};
  }

  [[nodiscard]] CAL_CONSTEXPR_FN std::optional<Date> xihuitl_tonalpohualli_on_or_before(const Date& date, const aztec::Xihuitl& xihuitl, const aztec::Tonalpohualli& tonalpohualli) noexcept {
    const auto xihuitlCount = xihuitl.ordinal() + aztec::Xihuitl::correlation;
    const auto tonalpohualiCount = tonalpohualli.ordinal() + aztec::Tonalpohualli::correlation;
    const auto diff = xihuitlCount - tonalpohualiCount;
    if (math::mod(diff, 5) == 0) {
      return Date{math::mod_range(xihuitlCount + 365*diff, date.day, date.day-18980)};
    }
    return std::nullopt;
  }
}