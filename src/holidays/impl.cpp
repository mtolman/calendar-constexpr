#include "impl.h"

std::vector<calendars::Date> calendars::holidays::impl::julian_in_gregorian(calendars::JulianDate::MONTH julianMonth,
                                                                            int16_t                     julianDay,
                                                                            int64_t                     gregorianYear) {
  const auto range = GregorianDate::year_boundaries(gregorianYear);

  const auto janFirst = std::get<0>(range);
  const auto y        = janFirst.to<JulianDate>().year;
  const auto yPrime   = y == -1 ? 1 : y + 1;
  const auto date0    = Date{JulianDate{y, julianMonth, julianDay}};
  const auto date1    = Date{JulianDate{yPrime, julianMonth, julianDay}};

  auto result = std::vector<Date>{};

  if (date0 >= std::get<0>(range) && date0 <= std::get<1>(range)) {
    result.emplace_back(date0);
  }
  if (date1 >= std::get<0>(range) && date1 <= std::get<1>(range)) {
    result.emplace_back(date1);
  }

  return result;
}
