#include "copts.h"

#define CAL_COPTIC_HOLIDAY_DEF(NAME)                                                                                                                 \
  std::vector<calendars::Date> calendars::holidays::copts::gregorian::NAME(int64_t gregorianYear) {                                                    \
    CAL_CONSTEXPR_VAR auto cd = calendars::holidays::copts::NAME(1).to<CopticDate>();                                                                 \
    return impl::two_date_in_gregorian<CopticDate>(cd.month, cd.day, gregorianYear);                                                                 \
  }

CAL_COPTIC_HOLIDAY_DEF(christmas)
CAL_COPTIC_HOLIDAY_DEF(building_of_the_cross)
CAL_COPTIC_HOLIDAY_DEF(jesus_circumcision)
CAL_COPTIC_HOLIDAY_DEF(epiphany)
CAL_COPTIC_HOLIDAY_DEF(marys_announcement)
CAL_COPTIC_HOLIDAY_DEF(jesus_transfiguration)

#undef CAL_COPTIC_HOLIDAY_DEF
