#include "islamic.h"

#define CAL_ISLAMIC_HOLIDAY_DEF(NAME)                                                                                                                \
  std::vector<calendars::Date> calendars::holidays::islamic::gregorian::NAME(int64_t gregorianYear) {                                                  \
    CAL_CONSTEXPR_VAR auto id = calendars::holidays::islamic::NAME(2021).to<IslamicDate>();                                                 \
    return impl::three_date_in_gregorian<IslamicDate>(id.month, id.day, gregorianYear);                                                    \
  }

CAL_ISLAMIC_HOLIDAY_DEF(new_year)
CAL_ISLAMIC_HOLIDAY_DEF(ashura)
CAL_ISLAMIC_HOLIDAY_DEF(mawlid)
CAL_ISLAMIC_HOLIDAY_DEF(lailat_al_miraj)
CAL_ISLAMIC_HOLIDAY_DEF(lailat_al_baraa)
CAL_ISLAMIC_HOLIDAY_DEF(ramadan)
CAL_ISLAMIC_HOLIDAY_DEF(lailat_al_kadr)
CAL_ISLAMIC_HOLIDAY_DEF(eid_ul_fitr)
CAL_ISLAMIC_HOLIDAY_DEF(eid_ul_adha)

#undef CAL_ISLAMIC_HOLIDAY_DEF
