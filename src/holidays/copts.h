#pragma once

#include "impl.h"
#include "../calendars/coptic_date.h"

/**
   * Holidays specific to the Coptic churches
 */
namespace calendars::holidays::copts {
  CAL_CONSTEXPR_FN Date christmas(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::KOIAK, 29}}; }

  CAL_CONSTEXPR_FN Date building_of_the_cross(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::THOOUT, 17}}; }

  CAL_CONSTEXPR_FN Date jesus_circumcision(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::TOBE, 6}}; }

  CAL_CONSTEXPR_FN Date epiphany(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::TOBE, 11}}; }

  CAL_CONSTEXPR_FN Date marys_announcement(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::PAREMOTEP, 29}}; }

  CAL_CONSTEXPR_FN Date jesus_transfiguration(int64_t copticYear) noexcept { return Date{CopticDate{copticYear, CopticDate::MONTH::MESORE, 13}}; }

  namespace gregorian {
    std::vector<Date> christmas(int64_t gregorianYear);
    std::vector<Date> building_of_the_cross(int64_t gregorianYear);
    std::vector<Date> jesus_circumcision(int64_t gregorianYear);
    std::vector<Date> epiphany(int64_t gregorianYear);
    std::vector<Date> marys_announcement(int64_t gregorianYear);
    std::vector<Date> jesus_transfiguration(int64_t gregorianYear);
  }  // namespace gregorian
}    // namespace copts