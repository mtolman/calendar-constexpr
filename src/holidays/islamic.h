#pragma once

#include "../calendars/islamic_date.h"
#include "impl.h"

/**
   * Holidays on the Arithmetic Islamic Calendar
 */
namespace calendars::holidays::islamic {

    CAL_CONSTEXPR_FN Date new_year(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::MUHARRAM, 1}};
    }

    CAL_CONSTEXPR_FN Date ashura(int64_t islamicYear) { return Date{IslamicDate{islamicYear, IslamicDate::MONTH::MUHARRAM, 10}}; }

    CAL_CONSTEXPR_FN Date mawlid(int64_t islamicYear) { return Date{IslamicDate{islamicYear, IslamicDate::MONTH::RABI_I, 12}}; }

    CAL_CONSTEXPR_FN Date lailat_al_miraj(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::RAJAB, 27}};
    }

    CAL_CONSTEXPR_FN Date lailat_al_baraa(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::SHABAN, 15}};
    }

    CAL_CONSTEXPR_FN Date ramadan(int64_t islamicYear) { return Date{IslamicDate{islamicYear, IslamicDate::MONTH::RAMADAN, 1}}; }

    CAL_CONSTEXPR_FN Date lailat_al_kadr(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::RAMADAN, 27}};
    }

    CAL_CONSTEXPR_FN Date eid_ul_fitr(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::SHAWWAL, 1}};
    }

    CAL_CONSTEXPR_FN Date eid_ul_adha(int64_t islamicYear) {
      return Date{IslamicDate{islamicYear, IslamicDate::MONTH::DHU_AL_HIJJA, 10}};
    }

    namespace gregorian {
      std::vector<Date> new_year(int64_t gregorianYear);
      std::vector<Date> ashura(int64_t gregorianYear);
      std::vector<Date> mawlid(int64_t gregorianYear);
      std::vector<Date> lailat_al_miraj(int64_t gregorianYear);
      std::vector<Date> lailat_al_baraa(int64_t gregorianYear);
      std::vector<Date> ramadan(int64_t gregorianYear);
      std::vector<Date> lailat_al_kadr(int64_t gregorianYear);
      std::vector<Date> eid_ul_fitr(int64_t gregorianYear);
      std::vector<Date> eid_ul_adha(int64_t gregorianYear);
    }  // namespace gregorian
}