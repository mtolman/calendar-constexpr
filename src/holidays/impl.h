#pragma once

#include <optional>
#include "../calendars/common.h"
#include "../calendars/julian_date.h"
#include "../calendars/gregorian_date.h"
#include "../calendars/coptic_date.h"

namespace calendars::holidays::impl {
  CAL_CONSTEXPR_FN Date first_week_day(DAY_OF_WEEK k, const Date& d) { return d.nth_week_day(1, k); }

  CAL_CONSTEXPR_FN Date last_week_day(DAY_OF_WEEK k, const Date& d) { return d.nth_week_day(-1, k); }

  template<typename T = Date, size_t... I>
  CAL_CONSTEXPR_FN std::array<Date, sizeof...(I)> day_sequence(const Date& startDate, std::index_sequence<I...> /* unused */) {
    return std::array<Date, sizeof...(I)>{startDate.add_days(static_cast<int64_t>(I))...};
  }

  std::vector<Date> julian_in_gregorian(JulianDate::MONTH julianMonth, int16_t julianDay, int64_t gregorianYear);

  template<typename T>
  std::vector<Date> two_date_in_gregorian(typename T::MONTH month, int16_t day, int64_t gregorianYear) {
    const auto boundaries = GregorianDate::year_boundaries(gregorianYear);
    const auto jan1       = std::get<0>(boundaries);
    const auto dec31      = std::get<1>(boundaries);
    const auto y          = jan1.to<T>().year;
    const auto d0         = Date{T{y, month, day}};
    const auto d1         = Date{T{y + 1, month, day}};

    std::vector<Date> res{};

    if (d0 >= jan1 && d0 <= dec31) {
      res.template emplace_back<>(d0);
    }
    if (d1 >= jan1 && d1 <= dec31) {
      res.template emplace_back<>(d1);
    }
    return res;
  }

  template<typename T>
  std::vector<Date> three_date_in_gregorian(typename T::MONTH month, int16_t day, int64_t gregorianYear) {
    const auto [ start, end ] = GregorianDate::year_boundaries(gregorianYear);
    const auto        y       = start.to<T>().year;
    const auto        date0   = Date{T{y, month, day}};
    const auto        date1   = Date{T{y + 1, month, day}};
    const auto        date2   = Date{T{y + 2, month, day}};
    std::vector<Date> res{};
    if (date0 >= start && date0 <= end) {
      res.emplace_back(date0);
    }
    if (date1 >= start && date1 <= end) {
      res.emplace_back(date1);
    }
    if (date2 >= start && date2 <= end) {
      res.emplace_back(date2);
    }
    return res;
  }
}  // namespace calendars::holidays::imp