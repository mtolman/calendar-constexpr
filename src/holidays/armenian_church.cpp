#include "armenian_church.h"

std::vector<calendars::Date> calendars::holidays::armenian_church::gregorian::christmas_jerusalem(int64_t gregorianYear) {
  const auto jd = julian::christmas_jerusalem(1).to<JulianDate>();
  return impl::julian_in_gregorian(jd.month, jd.day, gregorianYear);
}
