#pragma once

#include "impl.h"
#include <vector>

namespace calendars::holidays::misc {
  std::vector<Date> unlucky_fridays_in_range(std::tuple<Date, Date> range);

  std::vector<Date> unlucky_fridays_in_year(int64_t year);
}