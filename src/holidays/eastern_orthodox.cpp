#include "eastern_orthodox.h"

#define CAL_FIXED_JULIAN_HOLIDAY(NAME)                                                                                                               \
  std::vector<calendars::Date> calendars::holidays::eastern_orthodox::gregorian::NAME(int64_t gregorianYear) {                                         \
    const auto jd = julian::NAME(1).to<JulianDate>();                                                                                                \
    return impl::julian_in_gregorian(jd.month, jd.day, gregorianYear);                                                                               \
  }

CAL_FIXED_JULIAN_HOLIDAY(christmas_day)
CAL_FIXED_JULIAN_HOLIDAY(nativity_of_the_virgin_mary)
CAL_FIXED_JULIAN_HOLIDAY(elevation_of_the_life_giving_cross)
CAL_FIXED_JULIAN_HOLIDAY(presentation_of_the_virgin_mary_in_the_temple)
CAL_FIXED_JULIAN_HOLIDAY(theophany)
CAL_FIXED_JULIAN_HOLIDAY(presentation_of_christ_in_the_temple)
CAL_FIXED_JULIAN_HOLIDAY(the_annunciation)
CAL_FIXED_JULIAN_HOLIDAY(the_transfiguration)
CAL_FIXED_JULIAN_HOLIDAY(the_repose_of_the_virgin_mary)
CAL_FIXED_JULIAN_HOLIDAY(easter)

#undef CAL_FIXED_JULIAN_HOLIDAY



std::vector<calendars::Date> calendars::holidays::eastern_orthodox::gregorian::the_fast_of_the_repose_of_the_virgin_mary(int64_t gregorianYear) {
  std::vector<Date> res{};
  res.reserve(14);

  auto dates = julian::the_fast_of_the_repose_of_the_virgin_mary(1);
  for (decltype(auto) d : dates) {
    auto jd = d.to<JulianDate>();
    auto v  = impl::julian_in_gregorian(jd.month, jd.day, gregorianYear);
    for (decltype(auto) gd : v) {
      res.emplace_back(gd);
    }
  }

  res.shrink_to_fit();
  return res;
}

std::vector<calendars::Date> calendars::holidays::eastern_orthodox::gregorian::the_40_day_christmas_fast(int64_t gregorianYear) {
  std::vector<Date> res{};
  res.reserve(40);

  auto dates = julian::the_40_day_christmas_fast(1);
  for (decltype(auto) d : dates) {
    auto jd = d.to<JulianDate>();
    auto v  = impl::julian_in_gregorian(jd.month, jd.day, gregorianYear);
    for (decltype(auto) gd : v) {
      res.emplace_back(gd);
    }
  }

  res.shrink_to_fit();
  return res;
}
