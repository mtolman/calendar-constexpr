#pragma once

#include "impl.h"

/**
   * Common multi-denominational or catholic Christian holidays defined in terms of the Gregorian Calendar.
   * For Eastern Orthodox holidays defined in terms of the Julian calendars, use the eastern_orthodox namespace
 */
namespace calendars::holidays::christian {
  CAL_CONSTEXPR_FN Date christmas_day(int64_t year) { return Date{GregorianDate{year, GregorianDate::MONTH::DECEMBER, 25}}; }

  CAL_CONSTEXPR_FN Date christmas_eve_day(int64_t year) { return Date{GregorianDate{year, GregorianDate::MONTH::DECEMBER, 24}}; }

  CAL_CONSTEXPR_FN Date advent_sunday(int64_t year) {
    return Date{GregorianDate{year, GregorianDate::MONTH::NOVEMBER, 30}}.day_of_week_nearest(DAY_OF_WEEK::SUNDAY);
  }

  CAL_CONSTEXPR_FN Date epiphany(int64_t year) {
    return impl::first_week_day(DAY_OF_WEEK::SUNDAY, Date{GregorianDate{year, GregorianDate::MONTH::JANUARY, 2}});
  }

  /**
     * Uses the gregoran calendars method of calculation used by Catholic and Protestant churches
     * Note: Does not use the astronomical method based on full moons
     * @param year
     * @return
   */
  CAL_CONSTEXPR_FN Date easter(int64_t year) {
    const auto century   = math::floor(static_cast<double>(year) / 100.0) + 1;
    const auto yearMod19 = math::mod(year, 19);
    const auto shiftedEpact =
        math::mod(14 + 11 * yearMod19 - math::floor<double, int64_t>(century * 0.75) + math::floor<double, int64_t>((5 + 8 * century) / 25.0), 30);
    const auto adjustedEpact = (shiftedEpact == 0 || (shiftedEpact == 1 && 10 < yearMod19)) ? shiftedEpact + 1 : shiftedEpact;
    const auto paschalMoon   = Date{GregorianDate{year, GregorianDate::MONTH::APRIL, 19}}.sub_days(adjustedEpact);
    return paschalMoon.day_of_week_after(DAY_OF_WEEK::SUNDAY);
  }

  namespace impl {
    template<int DAYS>
    CAL_CONSTEXPR_FN Date days_from_easter(int64_t year) {
      return easter(year).add_days(DAYS);
    }

    template<int StartOffset, size_t... Indices>
    CAL_CONSTEXPR_FN std::array<Date, sizeof...(Indices)> impl_range_from_easter(int64_t year, std::index_sequence<Indices...> /* unused */) {
      return std::array{days_from_easter<static_cast<int>(Indices) + StartOffset>(year)...};
    }

    template<int StartOffset, int LEN>
    CAL_CONSTEXPR_FN std::array<Date, LEN> range_from_easter(int64_t year) {
      return impl_range_from_easter<StartOffset>(year, std::make_index_sequence<LEN>());
    }
  }  // namespace impl

  CAL_CONSTEXPR_FN Date septuagesima_sunday(int64_t year) { return impl::days_from_easter<-63>(year); }

  CAL_CONSTEXPR_FN Date sexagesima_sunday(int64_t year) { return impl::days_from_easter<-56>(year); }

  CAL_CONSTEXPR_FN Date shrove_sunday(int64_t year) { return impl::days_from_easter<-49>(year); }

  CAL_CONSTEXPR_FN Date shrove_monday(int64_t year) { return impl::days_from_easter<-48>(year); }

  CAL_CONSTEXPR_FN Date shrove_tuesday(int64_t year) { return impl::days_from_easter<-47>(year); }

  CAL_CONSTEXPR_FN Date mardi_gras(int64_t year) { return impl::days_from_easter<-47>(year); }

  CAL_CONSTEXPR_FN Date ash_wednesday(int64_t year) { return impl::days_from_easter<-46>(year); }

  CAL_CONSTEXPR_FN Date passion_sunday(int64_t year) { return impl::days_from_easter<-14>(year); }

  CAL_CONSTEXPR_FN Date palm_sunday(int64_t year) { return impl::days_from_easter<-7>(year); }

  CAL_CONSTEXPR_FN Date holy_thursday(int64_t year) { return impl::days_from_easter<-3>(year); }

  CAL_CONSTEXPR_FN Date maundy_thursday(int64_t year) { return impl::days_from_easter<-3>(year); }

  CAL_CONSTEXPR_FN Date good_friday(int64_t year) { return impl::days_from_easter<-2>(year); }

  CAL_CONSTEXPR_FN Date rogation_sunday(int64_t year) { return impl::days_from_easter<35>(year); }

  CAL_CONSTEXPR_FN Date ascension_day(int64_t year) { return impl::days_from_easter<39>(year); }

  CAL_CONSTEXPR_FN Date pentecost(int64_t year) { return impl::days_from_easter<49>(year); }

  CAL_CONSTEXPR_FN Date whitsunday(int64_t year) { return impl::days_from_easter<49>(year); }

  CAL_CONSTEXPR_FN Date whit_monday(int64_t year) { return impl::days_from_easter<50>(year); }

  CAL_CONSTEXPR_FN Date trinity_sunday(int64_t year) { return impl::days_from_easter<56>(year); }

  CAL_CONSTEXPR_FN Date corpus_christi(int64_t year) { return impl::days_from_easter<60>(year); }

  CAL_CONSTEXPR_FN Date corpus_christi_us_catholic(int64_t year) { return impl::days_from_easter<63>(year); }

  CAL_CONSTEXPR_FN std::array<Date, 40> the_40_days_of_lent(int64_t year) { return impl::range_from_easter<-46, 40>(year); }
}  // namespace christian