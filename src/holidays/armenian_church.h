#pragma once

#include "impl.h"

/**
 * Holds holidays specific to the Armenian Church
 */
namespace calendars::holidays::armenian_church {
  namespace julian {
    CAL_CONSTEXPR_FN Date christmas_jerusalem(int64_t julianYear) { return Date{JulianDate{julianYear, JulianDate::MONTH::JANUARY, 6}}; }
  }  // namespace julian

  namespace gregorian {
    std::vector<Date> christmas_jerusalem(int64_t gregorianYear);

    CAL_CONSTEXPR_FN Date christmas(int64_t gregorianYear) { return Date{GregorianDate{gregorianYear, GregorianDate::MONTH::JANUARY, 6}}; }
  }  // namespace gregorian
}  // namespace calendars::holidays::armenian_church
