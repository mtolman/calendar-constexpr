#include "hebrew.h"

std::vector<calendars::Date> calendars::holidays::hebrew::gregorian::sh_ela(int64_t gregorian_year) {
  return impl::two_date_in_gregorian<CopticDate>(static_cast<CopticDate::MONTH>(3), 26, gregorian_year);
}

std::vector<calendars::Date> calendars::holidays::hebrew::gregorian::birkath_ha_hama(int64_t gregorian_year) {
  auto dates = impl::two_date_in_gregorian<CopticDate>(static_cast<CopticDate::MONTH>(3), 30, gregorian_year);
  if (!dates.empty() && math::mod(dates[ 0 ].to<CopticDate>().year, 28) == 17) {
    return dates;
  }
  return {};
}

