#pragma once
#include <type_traits>

#ifdef __has_builtin
#define CAL_HAS_BUILTIN(fun) __has_builtin(fun)
#else
#define CAL_HAS_BUILTIN(fun) 0
#endif

#ifndef CAL_USE_CONSTEXPR
#if __cpp_lib_is_constant_evaluated >= 201811
#define CAL_USE_STD_IS_CONST_EVAL     1
#define CAL_USE_CONSTEXPR             CAL_USE_STD_IS_CONST_EVAL
#define CAL_USE_BUILTIN_IS_CONST_EVAL 0
#else
#define CAL_USE_BUILTIN_IS_CONST_EVAL CAL_HAS_BUILTIN(__builtin_is_constant_evaluated)
#define CAL_USE_CONSTEXPR             CAL_USE_BUILTIN_IS_CONST_EVAL
#define CAL_USE_STD_IS_CONST_EVAL     0
#endif
#endif

#ifndef CAL_CONSTEXPR_FN
#if CAL_USE_CONSTEXPR
#define CAL_CONSTEXPR_FN       constexpr
#define CAL_CONSTEXPR_VAR      constexpr
#define CAL_CONSTEXPR_CLASS_FN constexpr
#else
#define CAL_CONSTEXPR_FN  inline
#define CAL_CONSTEXPR_VAR const
#define CAL_CONSTEXPR_CLASS_FN
#endif
#endif