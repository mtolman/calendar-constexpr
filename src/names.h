#pragma once

#include <array>

#include "calendars.h"
#include "common.h"
#include "str.h"

#ifdef __cpp_lib_char8_t
#define CAL_CHAR_TYPE char8_t
#else
#define CAL_CHAR_TYPE char
#endif

namespace calendars::names {
  namespace egyptian {
    /**
     * Names of the Egyptian months
     */
    constexpr auto months = std::array<const CAL_CHAR_TYPE*, 13>{u8"Thoth",
                                                                 u8"Phaophi",
                                                                 u8"Athyr",
                                                                 u8"Choiak",
                                                                 u8"Tybi",
                                                                 u8"Mechir",
                                                                 u8"Phamenoth",
                                                                 u8"Pharmuthi",
                                                                 u8"Pachon",
                                                                 u8"Payni",
                                                                 u8"Epiphi",
                                                                 u8"Mesori",
                                                                 u8""};
  }  // namespace egyptian

  namespace armenian {
    /**
     * Names of the Armenian months
     */
    constexpr auto months = std::array<const CAL_CHAR_TYPE*, 13>{u8"Nawasardi",
                                                                 u8"Ho\u1E59i",
                                                                 u8"Sahmi",
                                                                 u8"Tr\u0113",
                                                                 u8"K'aloch",
                                                                 u8"Arach",
                                                                 u8"Mehekani",
                                                                 u8"Areg",
                                                                 u8"Ahekani",
                                                                 u8"Mareri",
                                                                 u8"Margach",
                                                                 u8"Hrotich",
                                                                 u8""};
  }  // namespace armenian

  namespace zoroastrian {
    /**
     * Names of the Zoroastrian months
     */
    constexpr auto months = std::array<const CAL_CHAR_TYPE*, 13>{u8"Farvardin",
                                                                 u8"Ardibehesht",
                                                                 u8"Khordad",
                                                                 u8"Tir",
                                                                 u8"Amardad",
                                                                 u8"Shehrevar",
                                                                 u8"Mehr",
                                                                 u8"Aban",
                                                                 u8"Azar",
                                                                 u8"Dae",
                                                                 u8"Bahman",
                                                                 u8"Asfand",
                                                                 u8""};

    /**
     * Names of the Zoroastrian days
     */
    constexpr auto days = std::array<const CAL_CHAR_TYPE*, 30>{u8"Hormuz",
                                                               u8"Bahman",
                                                               u8"Ord\u012bbehesht",
                                                               u8"Shahr\u012bvar",
                                                               u8"Esfand\u0101rmud",
                                                               u8"Xord\u0101d",
                                                               u8"Mord\u0101d",
                                                               u8"Diy be \u0100zar",
                                                               u8"\u0100zar",
                                                               u8"\u0100b\u0101n",
                                                               u8"Xor",
                                                               u8"M\u0101h",
                                                               u8"T\u012br",
                                                               u8"Goosh",
                                                               u8"Diy be Mehr",
                                                               u8"Mehr",
                                                               u8"Sor\u016bsh",
                                                               u8"Rashn",
                                                               u8"Favard\u012bn",
                                                               u8"Bahr\u0101m",
                                                               u8"R\u0101m",
                                                               u8"B\u0101d",
                                                               u8"Diy be D\u012bn",
                                                               u8"D\u012bn",
                                                               u8"Ard",
                                                               u8"Asht\u0101d",
                                                               u8"Asm\u0101n",
                                                               u8"Z\u0101my\u0101d",
                                                               u8"M\u0101resfand",
                                                               u8"An\u012br\u0101n"};

    /**
     * Names of the Epagomanae days
     */
    const auto epagomanaeDays = std::array<const CAL_CHAR_TYPE*, 5>{u8"Ahnad", u8"Ashnad", u8"Esfand\u0101rmud", u8"Axshatar", u8"Behesht"};
  }  // namespace zoroastrian

  namespace akan {
    /**
     * Akan day prefix names
     */
    constexpr auto prefixNames = std::array<const CAL_CHAR_TYPE*, 6>{u8"Nwona", u8"Nkyi", u8"Kuru", u8"Kwa", u8"Mono", u8"Fo"};

    /**
     * Akan day stem neames
     */
    constexpr auto stemNames = std::array<const CAL_CHAR_TYPE*, 7>{u8"wukuo", u8"yaw", u8"fie", u8"memene", u8"kwasi", u8"dwo", u8"bene"};

    /**
     * Converts an AkanDay to it's name
     * @param akanDay
     * @return
     */
    inline std::string names_from_akan_day(const AkanDay& akanDay) {
      std::string res = str::from_u8string(prefixNames.at(akanDay.prefix));
      return res + str::from_u8string(stemNames.at(akanDay.stem));
    }
  }  // namespace akan

  namespace icelandic {
    constexpr auto days = std::array<const CAL_CHAR_TYPE*, 7>{
        u8"Sunnudagur", u8"M\u00e1nudagur", u8"\u00DEri\u00f0judagur", u8"Mi\u00f0vikudagur", u8"Fimmtudagur", u8"F\u00f6studagur", u8"Laugardagur"};

    constexpr auto months = std::array<const CAL_CHAR_TYPE*, 12>{
        u8"Harpa",
        u8"Skerpla",
        u8"S\u00F3lm\u00E1nu\u00f0ur",
        u8"Heyannir",
        u8"Tv\u00EDm\u00E1nu\u00f0ur",
        u8"Haustm\u00E1nu\u00f0ur",
        u8"Gorm\u00E1nu\u00f0ur",
        u8"\u00DDlir",
        u8"M\u00F6rsugur",
        u8"\u00DEorri",
        u8"G\u00F3a",
        u8"Einm\u00E1nu\u00f0ur",
    };
  }  // namespace icelandic
}  // namespace calendars::names
