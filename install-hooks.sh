#!/usr/bin/env bash
chmod a+x pre-commit
chmod a+x pre-push

cp ./pre-commit .git/hooks/
cp ./pre-push .git/hooks/
