#include "common.h"

TEST_SUITE("Christian Holidays") {
  TEST_CASE("Christmas Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::DECEMBER, 25}};
      REQUIRE_EQ(d, h::christian::christmas_day(year));
    });
  }

  TEST_CASE("Easter") {
    rc::check([](int32_t year) {
      auto easter = h::christian::easter(year);
      REQUIRE_EQ(easter.day_of_week(), c::DAY_OF_WEEK::SUNDAY);
      REQUIRE_GE(easter, c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::MARCH, 22}});
      REQUIRE_LE(easter, c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::APRIL, 25}});
    });
  }

  TEST_CASE("Christmas Eve Day") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::DECEMBER, 24}};
      REQUIRE_EQ(d, h::christian::christmas_eve_day(year));
    });
  }

  TEST_CASE("Advent Sunday") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::NOVEMBER, 30}};
      auto a = h::christian::advent_sunday(year);
      REQUIRE_LE(calendars::math::abs(d.day_difference(a)), 3);
      REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::SUNDAY);
    });
  }

  TEST_CASE("Epiphany") {
    rc::check([](int32_t year) {
      auto d = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::JANUARY, 2}};
      auto a = h::christian::epiphany(year);
      REQUIRE_LE(d, a);
      REQUIRE_GE(d.add_days(7), a);
      REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::SUNDAY);
    });
  }
}
