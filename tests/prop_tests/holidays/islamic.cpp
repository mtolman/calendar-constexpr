#include "common.h"

TEST_SUITE("Islamic Holidays") {
  TEST_CASE("New Year") {
    using D = c::IslamicDate;

    rc::check([](int64_t year) {
      const auto date = h::islamic::new_year(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::MUHARRAM, 1}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::new_year(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::MUHARRAM);
        REQUIRE_EQ(date.day, 1);
      }
    });
  }

  TEST_CASE("Ashura") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::ashura(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::MUHARRAM, 10}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::ashura(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::MUHARRAM);
        REQUIRE_EQ(date.day, 10);
      }
    });
  }

  TEST_CASE("Mawlid") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::mawlid(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::RABI_I, 12}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::mawlid(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::RABI_I);
        REQUIRE_EQ(date.day, 12);
      }
    });
  }

  TEST_CASE("lailat_al_miraj") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::lailat_al_miraj(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::RAJAB, 27}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_miraj(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::RAJAB);
        REQUIRE_EQ(date.day, 27);
      }
    });
  }

  TEST_CASE("lailat_al_baraa") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::lailat_al_baraa(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::SHABAN, 15}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_baraa(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::SHABAN);
        REQUIRE_EQ(date.day, 15);
      }
    });
  }

  TEST_CASE("ramadan") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::ramadan(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::RAMADAN, 1}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::ramadan(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::RAMADAN);
        REQUIRE_EQ(date.day, 1);
      }
    });
  }

  TEST_CASE("lailat_al_kadr") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::lailat_al_kadr(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::RAMADAN, 27}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::lailat_al_kadr(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::RAMADAN);
        REQUIRE_EQ(date.day, 27);
      }
    });
  }

  TEST_CASE("eid_ul_fitr") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::eid_ul_fitr(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::SHAWWAL, 1}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::eid_ul_fitr(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::SHAWWAL);
        REQUIRE_EQ(date.day, 1);
      }
    });
  }

  TEST_CASE("eid_ul_adha") {
    using D = c::IslamicDate;

    rc::check([](int32_t year) {
      const auto date = h::islamic::eid_ul_adha(year);
      REQUIRE_EQ(date, c::Date{D{year, D::MONTH::DHU_AL_HIJJA, 10}});
    });

    rc::check([](int32_t year) {
      const auto dates = h::islamic::gregorian::eid_ul_adha(year);
      for (auto d : dates) {
        auto date = d.to<D>();
        REQUIRE_EQ(date.month, D::MONTH::DHU_AL_HIJJA);
        REQUIRE_EQ(date.day, 10);
      }
    });
  }
}
