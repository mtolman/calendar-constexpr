#include "common.h"

TEST_SUITE("Misc Holidays") {
  TEST_CASE("Unlucky Friday") {
    rc::check([](int32_t year) {
      auto fridays = h::misc::unlucky_fridays_in_year(year);
      for (decltype(auto) friday : fridays) {
        REQUIRE_EQ(friday.day_of_week(), c::DAY_OF_WEEK::FRIDAY);
        REQUIRE_EQ(friday.to<c::GregorianDate>().day, 13);
      }
    });
  }
}