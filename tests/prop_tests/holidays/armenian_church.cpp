#include "common.h"

TEST_SUITE("Armenian Church Holidays") {
  TEST_CASE("Christmas Day (Jerusalem)") {
    SUBCASE("Julian") {
      rc::check([](int32_t year) {
        auto holiday = h::armenian_church::julian::christmas_jerusalem(year);
        REQUIRE_EQ(holiday.to<c::JulianDate>(), c::JulianDate{year, c::JulianDate::MONTH::JANUARY, 6});
      });
    }

    SUBCASE("Gregorian") {
      rc::check([](int32_t year) {
        auto holiday = h::armenian_church::gregorian::christmas_jerusalem(year);

        for (decltype(auto) date : holiday) {
          auto julianDate = date.to<c::JulianDate>();
          REQUIRE_EQ(julianDate.month, c::JulianDate::MONTH::JANUARY);
          REQUIRE_EQ(julianDate.day, 6);
          REQUIRE_EQ(date.to<c::GregorianDate>().year, year);
        }
      });
    }
  }

  TEST_CASE("Christmas Day (General)") {
    rc::check([](int32_t year) {
      auto holiday = h::armenian_church::gregorian::christmas(year);
      REQUIRE_EQ(holiday, c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::JANUARY, 6}});
    });
  }
}
