#include <common_test_headers.h>
#include <rapidcheck.h>

#include "common.hpp"
#include <doctest.h>
#include "src/cal_math.h"

TEST_SUITE("Math") {
  TEST_CASE("Floor") {
    CHECK(rc::check([](double num) {
      const auto floored = calendars::math::floor(num);
      return CHECK_RET((floored <= num) && floored >= (num - 1) && floored == std::floor(num), num);
    }));
  }

  TEST_CASE("Ceil") {
    auto res = rc::check([](double num) {
      const auto ceiled = calendars::math::ceil(num);
      return CHECK_RET(ceiled >= num && ceiled <= (num + 1) && ceiled == std::ceil(num), num);
    });
    CHECK(res);
  }

  TEST_CASE("Floor constexpr") {
    CHECK(rc::check([](double num) {
      const auto floored = calendars::math::floor_constexpr(num);
      return CHECK_RET((floored <= num) && floored >= (num - 1) && floored == std::floor(num), num);
    }));
  }

  TEST_CASE("Ceil constexpr") {
    auto res = rc::check([](double num) {
      const auto ceiled = calendars::math::ceil_constexpr(num);
      return CHECK_RET(ceiled >= num && ceiled <= (num + 1) && ceiled == std::ceil(num), num);
    });
    CHECK(res);
  }

  TEST_CASE("Round") {
    {
      double     num      = 5721421830524369;
      const auto rounded  = calendars::math::round(num);
      const auto sRounded = static_cast<int64_t>(std::round(num));
      CHECK(rounded == sRounded);
    }

    auto res = rc::check([](double num) {
      const auto rounded  = calendars::math::round<double, double>(num);
      const auto sRounded = std::round(num);
      return CHECK_RET(rounded == sRounded, rounded);
    });
    CHECK(res);
  }

  TEST_CASE("Mod") {
    auto res = rc::check([](int32_t a, int32_t b) {
      if (b == std::numeric_limits<int32_t>::min()) {
        b = 10;
      }
      else if (b < 0) {
        b = -b;
      }
      else if (b == 1 || b == -1) {
        b = 12 * b;
      }
      else if (b == 0) {
        b = 19;
      }

      auto r = calendars::math::mod(a, b);

      return CHECK_RETB(std::abs(r) < std::abs(b) && std::abs(r) >= std::abs(0) && (calendars::math::sign(r) == calendars::math::sign(b) || r == 0));
    });
    CHECK(res);
  }

  TEST_CASE("amod") {
    auto res = rc::check([](int32_t a, int32_t b) {
      if (b == std::numeric_limits<int32_t>::min()) {
        b = 10;
      }
      else if (b < 0) {
        b = -b;
      }
      else if (b == 1 || b == -1) {
        b = 12 * b;
      }
      else if (b == 0) {
        b = 19;
      }

      auto r = calendars::math::amod(a, b);

      return CHECK_RETB(std::abs(r) <= std::abs(b) && std::abs(r) > std::abs(0) && (calendars::math::sign(r) == calendars::math::sign(b) || r == 0));
    });
    CHECK(res);
  }

  TEST_CASE("gcd") {
    auto res = rc::check([](uint32_t a, uint32_t b) {
      if (a == 0) {
        a = 1;
      }
      if (b == 0) {
        b = 1;
      }
      auto r = calendars::math::gcd(a, b);
      return CHECK_RETB(r <= std::max(a, b) && r >= 1);
    });
    CHECK(res);
  }

  TEST_CASE("lcm") {
    auto res = rc::check([](int32_t a, int32_t b) {
      if (a == 0) {
        a = 1;
      }
      if (b == 0) {
        b = 1;
      }
      auto aLong = static_cast<int64_t>(std::abs(a));
      auto bLong = static_cast<int64_t>(std::abs(b));
      auto r     = calendars::math::lcm(aLong, bLong);
      return CHECK_RETB(r >= std::min(aLong, bLong) && r <= aLong * bLong);
    });
    CHECK(res);
  }

  TEST_CASE("Mod Range") {
    auto res = rc::check([](int32_t a, int32_t b, int32_t c) {
      if (b == std::numeric_limits<int32_t>::min()) {
        b = 10;
      }
      else if (b < 0) {
        b = -b;
      }
      else if (b == 1 || b == -1) {
        b = 12 * b;
      }
      else if (b == 0) {
        b = 19;
      }

      if (c == std::numeric_limits<int32_t>::min()) {
        c = 10;
      }
      else if (c < 0) {
        c = -c;
      }
      else if (c == 1 || c == -1) {
        c = 12 * c;
      }

      if (c == b) {
        c += 1;
      }

      if ((calendars::math::sign(c) == calendars::math::sign(b) && std::abs(c) < std::abs(b))
          || (calendars::math::sign(c) != calendars::math::sign(b) && c < b)) {
        std::swap(c, b);
      }

      auto r = calendars::math::mod_range(a, b, c);

      return CHECK_RETB(std::abs(r) < std::abs(c) && std::abs(r) >= std::abs(b));
    });
    CHECK(res);
  }

  TEST_CASE("Date Arithmetic") {
    SUBCASE("Addition to int32") {
      SUBCASE("Cummutative") {
        CHECK(rc::check([](int32_t l, int32_t right) {
          auto left = calendars::Date{l};

          auto res1 = left.add_days(right);
          auto res2 = calendars::Date{static_cast<int64_t>(right) + left.day};

          return CHECK_RETB(res1 == res2);
        }));
      }

      SUBCASE("Add1TwiceIsAdd2Property") {
        CHECK(rc::check([](int32_t l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int32_t one = 1;
          int32_t two = 2;

          auto res1 = left.add_days(one).add_days(one);
          auto res2 = left.add_days(two);

          return CHECK_RETB(res1 == res2);
        }));
      }

      SUBCASE("Associative") {
        CHECK(rc::check([](int32_t x, int32_t y) {
          auto X = calendars::Date{static_cast<double>(x)};
          auto Y = calendars::Date{static_cast<double>(y)};

          int32_t one = 1;

          auto res1 = X.add_days(Y.add_days(one).day);
          auto res2 = X.add_days(Y.day).add_days(one);

          return CHECK_RETB(res1 == res2);
        }));
      }

      SUBCASE("Identity") {
        CHECK(rc::check([](double l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int32_t zero = 0;

          return CHECK_RETB(left == left.add_days(zero));
        }));
      }
    }

    SUBCASE("Subtraction with int32") {
      SUBCASE("Subtract1TwiceIsSubtract2Property") {
        CHECK(rc::check([](int32_t l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int32_t one = 1;
          int32_t two = 2;

          auto res1 = left.sub_days(one).sub_days(one);
          auto res2 = left.sub_days(two);

          return CHECK_RETB(res1 == res2);
        }));
      }

      SUBCASE("Identity") {
        CHECK(rc::check([](double l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int32_t zero = 0;

          return CHECK_RETB(left == left.sub_days(zero));
        }));
      }
    }

    SUBCASE("Addition to int64") {
      SUBCASE("Subtract1TwiceIsSubtract2Property") {
        CHECK(rc::check([](int32_t l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int64_t one = 1;
          int64_t two = 2;

          auto res1 = left.sub_days(one).sub_days(one);
          auto res2 = left.sub_days(two);

          return CHECK_RETB(res1 == res2);
        }));
      }

      SUBCASE("Identity") {
        CHECK(rc::check([](double l) {
          auto left = calendars::Date{static_cast<double>(l)};

          int32_t zero = 0;

          return CHECK_RETB(left == left.sub_days(zero));
        }));
      }

      SUBCASE("Day difference") {
        rc::check([](int64_t dayL, int64_t dayR) { REQUIRE_EQ(calendars::Date{dayL}.day_difference(calendars::Date{dayR}), dayL - dayR); });
      }
    }
  }

  TEST_CASE("Gregorian") {
    SUBCASE("Leap Year") {
      rc::check([](int32_t year) {
        const auto expected = calendars::math::mod(year, 4) == 0 && (calendars::math::mod(year, 100) != 0 || calendars::math::mod(year, 400) == 0);
        REQUIRE_EQ(expected, calendars::GregorianDate::is_leap_year(year));
      });
    }

    SUBCASE("Day difference") {
      rc::check([](int32_t yearL, int16_t monthL, uint16_t dayL, int32_t yearR, int16_t monthR, uint16_t dayR) {
        auto left  = to_gregorian_normalized(yearL, monthL, dayL);
        auto right = to_gregorian_normalized(yearR, monthR, dayR);

        REQUIRE_EQ(left.day_difference(right), calendars::Date::from(left).day_difference(calendars::Date::from(right)));
      });
    }

    SUBCASE("Days in Year") {
      rc::check([](int32_t year, int16_t month, uint16_t day) {
        auto date = to_gregorian_normalized(year, month, day);
        const auto expected = calendars::GregorianDate::is_leap_year(year) ? 366 : 365;

        REQUIRE_EQ(date.day_number() + date.days_remaining(), expected);
        REQUIRE((date.day_number() != date.days_remaining() || (expected == 366 && date.day_number() == 366 / 2)));
      });
    }
  }
}
