#include "common.h"
#include "src/calendars/moment.h"

TEST_SUITE("Moment") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::Moment>()).day, day); });
  }

  TEST_CASE("Self-conversions") {
    CHECK(rc::check([](int32_t seconds) {
      return CHECK_RET(calendars::Moment::from(calendars::Moment{static_cast<double>(seconds)})
                           == calendars::Moment::from(calendars::Moment{static_cast<double>(seconds)}),
                       seconds);
    }));
  }
}


