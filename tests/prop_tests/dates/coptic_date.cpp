#include "common.h"
#include "src/calendars/coptic_date.h"

TEST_SUITE("Coptic Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::CopticDate>()).day, day); });
  }
}
