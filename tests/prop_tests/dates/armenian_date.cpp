#include "common.h"
#include "src/calendars/armenian_date.h"

TEST_SUITE("Armenian Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::ArmenianDate>()).day, day); });
  }
}
