#include <common_test_headers.h>
#include <doctest.h>
#include <rapidcheck/include/rapidcheck.h>
#include "src/calendars.h"
#include "tests/prop_tests/common.hpp"

TEST_SUITE("Arbitrary Conversions") {
  TEST_CASE("Convert") {
    rc::check([](int32_t day, ConversionTarget target) {
      return CHECK_RET(
          convert_date(calendars::Date{day}, target) == calendars::Date{day}, std::to_string(day) + ", " + std::string(target_name(target))
      );
    });
  }

}
