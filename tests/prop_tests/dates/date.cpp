#include "common.h"
#include "src/calendars/date.h"

TEST_SUITE("Date") {
  TEST_CASE("Comparisons") {
    REQUIRE(calendars::Date{}.day == calendars::Date::epoch);

    auto res = rc::check([](int32_t left, int32_t right) {
      calendars::Date leftDate{calendars::Date{static_cast<double>(left)}};
      calendars::Date rightDate{calendars::Date{static_cast<double>(right)}};

      bool okay = true;
      if (left == right) {
        ASSERT(leftDate <= rightDate)
        ASSERT(leftDate == rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else if (left < right) {
        ASSERT(leftDate < rightDate)
        ASSERT(leftDate <= rightDate)
        ASSERT_NOT(leftDate == rightDate)
        ASSERT_NOT(leftDate >= rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else {
        ASSERT_NOT(leftDate <= rightDate)
        ASSERT_NOT(leftDate == rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT(leftDate > rightDate)
      }
      return okay;
    });
    CHECK(res);
  }

  TEST_CASE("Conversions") {
    rc::check([](int days) { return CHECK_RET(days == calendars::Date(days).day, days - calendars::Date::epoch); });

    {
      int days = -1;
      CHECK_RET(days == calendars::Date::from(calendars::Date(days).to<calendars::MicroUnixTimestamp>()).day, days - calendars::Date::epoch);
    }

    rc::check([](int days) {
      return CHECK_RET(days == calendars::Date::from(calendars::Date(days).to<calendars::MicroUnixTimestamp>()).day, days - calendars::Date::epoch);
    });
  }

  TEST_SUITE("Extra Methods") {

    TEST_CASE("positions_in_range") {
      auto result = calendars::Date::positions_in_range(3, 7, 2, calendars::Date{calendars::Date{12}}, calendars::Date{calendars::Date{33}});
      CHECK_EQ(result.size(), 3);

      CHECK_EQ(result[ 0 ], calendars::Date{calendars::Date{15}});
      CHECK_EQ(result[ 1 ], calendars::Date{calendars::Date{22}});
      CHECK_EQ(result[ 2 ], calendars::Date{calendars::Date{29}});

      result = calendars::Date::positions_in_range(3, 7, 0, calendars::Date{calendars::Date{12}}, calendars::Date{calendars::Date{33}});
      CHECK_EQ(result.size(), 3);

      CHECK_EQ(result[ 0 ], calendars::Date{calendars::Date{17}});
      CHECK_EQ(result[ 1 ], calendars::Date{calendars::Date{24}});
      CHECK_EQ(result[ 2 ], calendars::Date{calendars::Date{31}});
    }

    TEST_CASE("Day of week in range") {
      auto res = rc::check([](int32_t day) {
        auto d = calendars::Date{static_cast<double>(day)}.day_of_week();
        return CHECK_RET(static_cast<int32_t>(d) >= 0 && static_cast<int32_t>(d) <= 6, day);
      });
      CHECK(res);

      res = rc::check([](int32_t day) {
        auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week();
        return CHECK_RET(static_cast<int32_t>(d) >= 0 && static_cast<int32_t>(d) <= 6, day);
      });
      CHECK(res);
    }

    TEST_CASE("Day of week on or before") {
      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{static_cast<double>(day)}.day_of_week_on_or_before(day_of_week);
            const auto res = (d <= calendars::Date{static_cast<double>(day)} && d.day_of_week() == day_of_week
                              && d > calendars::Date{static_cast<double>(day - 7)});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }

      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week_on_or_before(day_of_week);
            const auto res = (d <= calendars::Date{calendars::Date{static_cast<double>(day)}} && d.day_of_week() == day_of_week
                              && d > calendars::Date{calendars::Date{static_cast<double>(day - 7)}});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }
    }

    TEST_CASE("Day of week before") {
      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{static_cast<double>(day)}.day_of_week_before(day_of_week);
            const auto res = (d < calendars::Date{static_cast<double>(day)} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{static_cast<double>(day - 7)});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }

      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week_before(day_of_week);
            const auto res = (d < calendars::Date{calendars::Date{static_cast<double>(day)}} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{calendars::Date{static_cast<double>(day - 7)}});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }
    }

    TEST_CASE("Day of week near") {
      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{static_cast<double>(day)}.day_of_week_nearest(day_of_week);
            const auto res = (d <= calendars::Date{static_cast<double>(day) + 3} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{static_cast<double>(day - 3)});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }

      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week_nearest(day_of_week);
            const auto res = (d <= calendars::Date{calendars::Date{static_cast<double>(day) + 3}} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{calendars::Date{static_cast<double>(day - 3)}});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }
    }

    TEST_CASE("Day of week after or on") {
      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{static_cast<double>(day)}.day_of_week_on_or_after(day_of_week);
            const auto res = (d < calendars::Date{static_cast<double>(day) + 7} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{static_cast<double>(day)});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }

      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week_on_or_after(day_of_week);
            const auto res = (d < calendars::Date{calendars::Date{static_cast<double>(day) + 7}} && d.day_of_week() == day_of_week
                              && d >= calendars::Date{calendars::Date{static_cast<double>(day)}});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }
    }

    TEST_CASE("Day of week after") {
      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{static_cast<double>(day)}.day_of_week_after(day_of_week);
            const auto res = (d <= calendars::Date{static_cast<double>(day) + 7} && d.day_of_week() == day_of_week
                              && d > calendars::Date{static_cast<double>(day)});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }

      {
        auto r = rc::check([](int32_t day) {
          bool ok = true;
          for (auto day_of_week : daysOfWeek) {
            auto       d   = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_week_after(day_of_week);
            const auto res = (d <= calendars::Date{calendars::Date{static_cast<double>(day) + 7}} && d.day_of_week() == day_of_week
                              && d > calendars::Date{calendars::Date{static_cast<double>(day)}});
            if (!res) {
              std::cout << "Failed input: " << day << " " << day_of_week_name(day_of_week) << "\n";
            }
            CHECK(res);
            RC_ASSERT(res);
            if (!res) {
              ok = false;
            }
          }
          return ok;
        });
        CHECK(r);
      }
    }

    TEST_CASE("Day of m-cycle in range") {
      {
        auto res = rc::check([](int32_t day, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto d = calendars::Date{static_cast<double>(day)}.day_of_m_cycle(m, o);
          return CHECK_RET(d >= 0 && d <= m, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.day_of_m_cycle(m, o);
          return CHECK_RET(d >= 0 && d <= m, day);
        });
        CHECK(res);
      }
    }

    TEST_CASE("Kth day of m-cycle in range on or before") {
      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{static_cast<double>(day)}.kth_day_of_m_cycle_on_or_before(k % m, m, o).day;
          return CHECK_RET(d >= c - (m - 1) && d <= c, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.kth_day_of_m_cycle_on_or_before(k % m, m, o).to<calendars::Date>().day;
          return CHECK_RET(d >= c - (m - 1) && d <= c, day);
        });
        CHECK(res);
      }
    }

    TEST_CASE("Kth day of m-cycle in range before") {
      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{static_cast<double>(day)}.kth_day_of_m_cycle_before(k % m, m, o).day;
          return CHECK_RET(d >= c - m && d < c, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.kth_day_of_m_cycle_before(k % m, m, o).to<calendars::Date>().day;
          return CHECK_RET(d >= c - m && d < c, day);
        });
        CHECK(res);
      }
    }

    TEST_CASE("Kth day of m-cycle in range after") {
      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{static_cast<double>(day)}.kth_day_of_m_cycle_after(k % m, m, o).day;
          return CHECK_RET(d > c && d <= c + m, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.kth_day_of_m_cycle_after(k % m, m, o).to<calendars::Date>().day;
          return CHECK_RET(d > c && d <= c + m, day);
        });
        CHECK(res);
      }
    }

    TEST_CASE("Kth day of m-cycle in range on or after") {
      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{static_cast<double>(day)}.kth_day_of_m_cycle_on_or_after(k % m, m, o).day;
          return CHECK_RET(d >= c && d < c + m, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.kth_day_of_m_cycle_on_or_after(k % m, m, o).to<calendars::Date>().day;
          return CHECK_RET(d >= c && d < c + m, day);
        });
        CHECK(res);
      }
    }

    TEST_CASE("Kth day of m-cycle in range nearest") {
      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{static_cast<double>(day)}.kth_day_of_m_cycle_nearest_to(k % m, m, o).day;
          return CHECK_RET(d >= c - m / 2 && d <= c + m / 2, day);
        });
        CHECK(res);
      }

      {
        auto res = rc::check([](int32_t day, uint8_t k, uint8_t m, int8_t o) {
          if (m < 2) {
            m = 2;
          }
          auto c = calendars::Date{static_cast<double>(day)}.day;
          auto d = calendars::Date{calendars::Date{static_cast<double>(day)}}.kth_day_of_m_cycle_nearest_to(k % m, m, o).to<calendars::Date>().day;
          return CHECK_RET(d >= c - m / 2 && d <= c + m / 2, day);
        });
        CHECK(res);
      }
    }
  }
}
