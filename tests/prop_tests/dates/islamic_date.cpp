#include "common.h"
#include "src/calendars/islamic_date.h"

TEST_SUITE("Islamic Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::IslamicDate>()).day, day); });
  }
}


