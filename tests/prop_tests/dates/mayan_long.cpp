#include "common.h"
#include "src/calendars/mayan/long_date.h"

TEST_SUITE("Mayan Long Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::mayan::LongDate>()).day, day); });
  }
}


