#include "common.h"
#include "src/calendars/zoroastrian_date.h"

TEST_SUITE("Zoroastrian Date") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::ZoroastrianDate>()).day, day); });
  }
}


