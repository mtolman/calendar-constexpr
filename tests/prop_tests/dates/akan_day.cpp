#include "common.h"
#include "src/calendars/akan_day.h"

TEST_SUITE("Akan Day") {
  TEST_SUITE("Extensions") {
    TEST_CASE("Akan day on or before") {
      auto akan = calendars::AkanDay{4, 5};
      CHECK_EQ(akan.prefix, 4);
      CHECK_EQ(akan.stem, 5);

      {
        auto res = rc::check([](int n, int32_t day) {
          auto date    = calendars::Date{day};
          auto newDate = calendars::akan_day_on_or_before(date, calendars::AkanDay{n});

          return CHECK_RET(newDate <= date && newDate >= date.sub_days(42), day);
        });
        CHECK(res);
      }
    }
  }
}
