#include "common.h"
#include "src/calendars/modified_julian_day.h"

TEST_SUITE("Modified Julian Day") {
  TEST_CASE("Conversions") {
    rc::check([](int32_t day) { REQUIRE_EQ(calendars::Date::from(calendars::Date{day}.to<calendars::ModifiedJulianDay>()).day, day); });
  }

  TEST_CASE("Comparisons") {
    auto res = rc::check([](int32_t left, int32_t right) {
      auto leftDate  = calendars::ModifiedJulianDay{static_cast<double>(left)};
      auto rightDate = calendars::ModifiedJulianDay{static_cast<double>(right)};

      bool okay = true;
      if (left == right) {
        ASSERT(leftDate <= rightDate)
        ASSERT(leftDate == rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else if (left < right) {
        ASSERT(leftDate < rightDate)
        ASSERT(leftDate <= rightDate)
        ASSERT_NOT(leftDate == rightDate)
        ASSERT_NOT(leftDate >= rightDate)
        ASSERT_NOT(leftDate > rightDate)
      }
      else {
        ASSERT_NOT(leftDate <= rightDate)
        ASSERT_NOT(leftDate == rightDate)
        ASSERT_NOT(leftDate < rightDate)
        ASSERT(leftDate >= rightDate)
        ASSERT(leftDate > rightDate)
      }
      return okay;
    });
    CHECK(res);
  }
}


