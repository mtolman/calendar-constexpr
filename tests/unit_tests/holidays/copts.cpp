#include <doctest.h>

#include "src/calendars.h"
#include "src/holidays.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("Coptic Holidays") {
  TEST_CASE("Christmas") {
    [](int64_t year) {
      const auto date = h::copts::christmas(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::KOIAK, 29}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::christmas(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::KOIAK);
        CHECK_EQ(date.day, 29);
      }
    }(2021);
  }

  TEST_CASE("Building of the Cross") {
    [](int64_t year) {
      const auto date = h::copts::building_of_the_cross(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::THOOUT, 17}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::building_of_the_cross(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::THOOUT);
        CHECK_EQ(date.day, 17);
      }
    }(2021);
  }

  TEST_CASE("Jesus Circumcision") {
    [](int64_t year) {
      const auto date = h::copts::jesus_circumcision(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::TOBE, 6}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::jesus_circumcision(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::TOBE);
        CHECK_EQ(date.day, 6);
      }
    }(2021);
  }

  TEST_CASE("Epiphany") {
    [](int64_t year) {
      const auto date = h::copts::epiphany(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::TOBE, 11}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::epiphany(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::TOBE);
        CHECK_EQ(date.day, 11);
      }
    }(2021);
  }

  TEST_CASE("Mary's Announcement") {
    [](int64_t year) {
      const auto date = h::copts::marys_announcement(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::PAREMOTEP, 29}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::marys_announcement(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::PAREMOTEP);
        CHECK_EQ(date.day, 29);
      }
    }(2021);
  }

  TEST_CASE("Jesus' Transfiguration") {
    [](int64_t year) {
      const auto date = h::copts::jesus_transfiguration(year);
      CHECK_EQ(date, c::Date{c::CopticDate{year, c::CopticDate::MONTH::MESORE, 13}});
    }(2021);

    [](int64_t year) {
      const auto dates = h::copts::gregorian::jesus_transfiguration(year);
      for (auto d : dates) {
        auto date = d.to<c::CopticDate>();
        CHECK_EQ(date.month, c::CopticDate::MONTH::MESORE);
        CHECK_EQ(date.day, 13);
      }
    }(2021);
  }
}
