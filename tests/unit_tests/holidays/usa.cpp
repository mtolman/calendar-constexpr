#include "common.h"
#include "src/holidays/usa.h"

namespace h = calendars::holidays;
namespace c = calendars;

TEST_SUITE("USA Holidays") {
  TEST_CASE("Independence Day") {
    auto year = 2018;
    auto d    = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::JULY, 4}};
    REQUIRE_EQ(d, h::usa::independence_day(year));
  }

  TEST_CASE("Labor Day") {
    auto year = 2018;
    auto d    = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::SEPTEMBER, 1}};
    auto a    = h::usa::labor_day(year);
    REQUIRE_LE(d, a);
    REQUIRE_GE(d.add_days(7), a);
    REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::MONDAY);
  }

  TEST_CASE("Memorial Day") {
    auto year = 2018;
    auto d    = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::MAY, 31}};
    auto a    = h::usa::memorial_day(year);
    REQUIRE_GE(d, a);
    REQUIRE_LE(d.sub_days(7), a);
    REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::MONDAY);
  }

  TEST_CASE("Election Day") {
    auto year = 2018;
    auto d    = c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::NOVEMBER, 2}};
    auto a    = h::usa::election_day(year);
    REQUIRE_LE(d, a);
    REQUIRE_GE(d.add_days(7), a);
    REQUIRE_EQ(a.day_of_week(), c::DAY_OF_WEEK::TUESDAY);
  }

  TEST_SUITE("Sample Data") {
    TEST_CASE("Independence Day") {
      for (auto year : sampleYears) {
        REQUIRE_EQ(c::holidays::usa::independence_day(year), (c::Date{c::GregorianDate{year, c::GregorianDate::MONTH::JULY, 4}}));
      }
    }

    TEST_CASE("Election Day") {
      const auto expectedDays =
          std::vector<int16_t>{7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5, 4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7,
                               6, 4, 3, 2, 8, 6, 5, 4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5,
                               4, 3, 8, 7, 6, 5, 3, 2, 8, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 8, 7, 6, 4, 3, 2, 8, 6, 5, 4, 3, 2, 8, 7, 6};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::election_day(sampleYears[ i ]),
                   (c::Date{c::GregorianDate{sampleYears[ i ], c::GregorianDate::MONTH::NOVEMBER, expectedDays[ i ]}}));
      }
    }

    TEST_CASE("Labor Day") {
      const auto expectedDays =
          std::vector<int16_t>{4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2, 1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4,
                               3, 1, 7, 6, 5, 3, 2, 1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2,
                               1, 7, 5, 4, 3, 2, 7, 6, 5, 4, 2, 1, 7, 6, 4, 3, 2, 1, 6, 5, 4, 3, 1, 7, 6, 5, 3, 2, 1, 7, 6, 5, 4, 3};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::labor_day(sampleYears[ i ]), (c::Date{c::GregorianDate{sampleYears[ i ], c::GregorianDate::MONTH::SEPTEMBER, expectedDays[ i ]}}));
      }
    }

    TEST_CASE("Memorial Day") {
      const auto expectedDays = std::vector<int16_t>{
          29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27, 26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29,
          28, 26, 25, 31, 30, 28, 27, 26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27,
          26, 25, 30, 29, 28, 27, 25, 31, 30, 29, 27, 26, 25, 31, 29, 28, 27, 26, 31, 30, 29, 28, 26, 25, 31, 30, 28, 27, 26, 25, 31, 30, 29, 28};
      for (size_t i = 0; i < expectedDays.size(); ++i) {
        REQUIRE_EQ(c::holidays::usa::memorial_day(sampleYears[ i ]), (c::Date{c::GregorianDate{sampleYears[ i ], c::GregorianDate::MONTH::MAY, expectedDays[ i ]}}));
      }
    }
  }
}
