#include <doctest.h>

#include "src/cal_math.h"
#include "src/calendars.h"

namespace m = calendars::math;
namespace c = calendars;

TEST_SUITE("calendars::math") {
  TEST_CASE("mod") {
    CHECK_EQ(m::mod(3, 7), 3);
    CHECK_EQ(m::mod(7, 3), 1);
    CHECK_EQ(m::mod(12, 7), 5);
    CHECK_EQ(m::mod(-3, 7), 4);
    CHECK_EQ(m::mod(3, -7), -4);
    CHECK_EQ(m::mod(-3, -7), -3);
  }

  TEST_CASE("amod") {
    CHECK_EQ(m::amod(3, 7), 3);
    CHECK_EQ(m::amod(7, 7), 7);
    CHECK_EQ(m::amod(0, 7), 7);
    CHECK_EQ(m::amod(7, 3), 1);
    CHECK_EQ(m::amod(12, 7), 5);
    CHECK_EQ(m::amod(-3, 7), 4);
    CHECK_EQ(m::amod(3, -7), -4);
    CHECK_EQ(m::amod(-3, -7), -3);
  }

  TEST_CASE("adjusted remainder") {
    CHECK_EQ(m::adjusted_rem_fun(3, 7), 3);
    CHECK_EQ(m::adjusted_rem_fun(7, 7), 7);
    CHECK_EQ(m::adjusted_rem_fun(0, 7), 7);
    CHECK_EQ(m::adjusted_rem_fun(7, 3), 1);
    CHECK_EQ(m::adjusted_rem_fun(12, 7), 5);
    CHECK_EQ(m::adjusted_rem_fun(-3, 7), 4);
    CHECK_EQ(m::adjusted_rem_fun(3, -7), -4);
    CHECK_EQ(m::adjusted_rem_fun(-3, -7), -3);
  }

  TEST_CASE("mod range") {
    CHECK_EQ(m::mod_range(2, 3, 7), 6);
    CHECK_EQ(m::mod_range(12, 2, 2), 12);
    CHECK_EQ(m::mod_range(5, 3, 7), 5);
    CHECK_EQ(m::mod_range(12, 3, 7), 4);
  }

  TEST_CASE("gcd") {
    CHECK_EQ(m::gcd(100, 50), 50);
    CHECK_EQ(m::gcd(13, 15), 1);
    CHECK_EQ(m::gcd(10, 15), 5);
    CHECK_EQ(m::gcd(24, 32), 8);
  }

  TEST_CASE("lcm") {
    CHECK_EQ(m::lcm(100, 50), 100);
    CHECK_EQ(m::lcm(13, 15), 195);
    CHECK_EQ(m::lcm(10, 15), 30);
    CHECK_EQ(m::lcm(24, 32), 96);
  }

  TEST_CASE("floor") {
    CHECK_EQ(m::floor(4.5), 4);
    CHECK_EQ(m::floor(4.9), 4);
    CHECK_EQ(m::floor(4.1), 4);
    CHECK_EQ(m::floor(14.5), 14);
    CHECK_EQ(m::floor(14.9), 14);
    CHECK_EQ(m::floor(14.1), 14);
    CHECK_EQ(m::floor(14.0), 14.0);
    CHECK_EQ(m::floor(-4.5), -5);
    CHECK_EQ(m::floor(-4.9), -5);
    CHECK_EQ(m::floor(-4.1), -5);
    CHECK_EQ(m::floor(-14.5), -15);
    CHECK_EQ(m::floor(-14.9), -15);
    CHECK_EQ(m::floor(-14.1), -15);
    CHECK_EQ(m::floor(-14.0), -14.0);

    {
      using T  = double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor(max), max);
    }

    {
      using T  = float;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor(max), max);
    }

#ifndef EMSCRIPTEN
    {
      using T  = long double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor(max), max);
      CHECK_EQ(m::floor(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor(max), max);
    }
#endif

    // Check for no-op on integers
    CHECK_EQ(m::floor(5), 5);
    CHECK_EQ(m::floor(-5), -5);
  }

  TEST_CASE("ceil") {
    CHECK_EQ(m::ceil(4.5), 5);
    CHECK_EQ(m::ceil(4.9), 5);
    CHECK_EQ(m::ceil(4.1), 5);
    CHECK_EQ(m::ceil(14.5), 15);
    CHECK_EQ(m::ceil(14.9), 15);
    CHECK_EQ(m::ceil(14.1), 15);
    CHECK_EQ(m::ceil(14.0), 14.0);
    CHECK_EQ(m::ceil(-4.5), -4);
    CHECK_EQ(m::ceil(-4.9), -4);
    CHECK_EQ(m::ceil(-4.1), -4);
    CHECK_EQ(m::ceil(-14.5), -14);
    CHECK_EQ(m::ceil(-14.9), -14);
    CHECK_EQ(m::ceil(-14.1), -14);
    CHECK_EQ(m::ceil(-14.0), -14.0);

    {
      using T  = double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil(max), max);
    }

    {
      using T  = float;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil(max), max);
    }

#ifndef EMSCRIPTEN
    {
      using T  = long double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil(max), max);
      CHECK_EQ(m::ceil(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil(max), max);
    }
#endif

    // Check for no-op on integers
    CHECK_EQ(m::ceil(5), 5);
    CHECK_EQ(m::ceil(-5), -5);
  }

  TEST_CASE("floor constexpr") {
    CHECK_EQ(m::floor_constexpr(4.5), 4);
    CHECK_EQ(m::floor_constexpr(4.9), 4);
    CHECK_EQ(m::floor_constexpr(4.1), 4);
    CHECK_EQ(m::floor_constexpr(14.5), 14);
    CHECK_EQ(m::floor_constexpr(14.9), 14);
    CHECK_EQ(m::floor_constexpr(14.1), 14);
    CHECK_EQ(m::floor_constexpr(14.0), 14.0);
    CHECK_EQ(m::floor_constexpr(-4.5), -5);
    CHECK_EQ(m::floor_constexpr(-4.9), -5);
    CHECK_EQ(m::floor_constexpr(-4.1), -5);
    CHECK_EQ(m::floor_constexpr(-14.5), -15);
    CHECK_EQ(m::floor_constexpr(-14.9), -15);
    CHECK_EQ(m::floor_constexpr(-14.1), -15);
    CHECK_EQ(m::floor_constexpr(-14.0), -14.0);

    {
      using T  = double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor_constexpr(max), max);
    }

    {
      using T  = float;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor_constexpr(max), max);
    }

#ifndef EMSCRIPTEN
    {
      using T  = long double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::floor_constexpr(max), max);
      CHECK_EQ(m::floor_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::floor_constexpr(max), max);
    }
#endif

    // Check for no-op on integers
    CHECK_EQ(m::floor_constexpr(5), 5);
    CHECK_EQ(m::floor_constexpr(-5), -5);
  }

  TEST_CASE("ceil constexpr") {
    CHECK_EQ(m::ceil_constexpr(4.5), 5);
    CHECK_EQ(m::ceil_constexpr(4.9), 5);
    CHECK_EQ(m::ceil_constexpr(4.1), 5);
    CHECK_EQ(m::ceil_constexpr(14.5), 15);
    CHECK_EQ(m::ceil_constexpr(14.9), 15);
    CHECK_EQ(m::ceil_constexpr(14.1), 15);
    CHECK_EQ(m::ceil_constexpr(14.0), 14.0);
    CHECK_EQ(m::ceil_constexpr(-4.5), -4);
    CHECK_EQ(m::ceil_constexpr(-4.9), -4);
    CHECK_EQ(m::ceil_constexpr(-4.1), -4);
    CHECK_EQ(m::ceil_constexpr(-14.5), -14);
    CHECK_EQ(m::ceil_constexpr(-14.9), -14);
    CHECK_EQ(m::ceil_constexpr(-14.1), -14);
    CHECK_EQ(m::ceil_constexpr(-14.0), -14.0);

    {
      using T  = double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil_constexpr(max), max);
    }

    {
      using T  = float;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil_constexpr(max), max);
    }

#ifndef EMSCRIPTEN
    {
      using T  = long double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max *= 2;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max /= 4;
      CHECK_EQ(m::ceil_constexpr(max), max);
      CHECK_EQ(m::ceil_constexpr(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::ceil_constexpr(max), max);
    }
#endif

    // Check for no-op on integers
    CHECK_EQ(m::ceil_constexpr(5), 5);
    CHECK_EQ(m::ceil_constexpr(-5), -5);
  }

  TEST_CASE("round") {
    CHECK_EQ(m::round(4.5), 5);
    CHECK_EQ(m::round(4.9), 5);
    CHECK_EQ(m::round(4.1), 4);
    CHECK_EQ(m::round(14.5), 15);
    CHECK_EQ(m::round(14.9), 15);
    CHECK_EQ(m::round(14.1), 14);
    CHECK_EQ(m::round(14.0), 14.0);
    CHECK_EQ(m::round(-4.5), -5);
    CHECK_EQ(m::round(-4.9), -5);
    CHECK_EQ(m::round(-4.1), -4);
    CHECK_EQ(m::round(-14.5), -15);
    CHECK_EQ(m::round(-14.9), -15);
    CHECK_EQ(m::round(-14.1), -14);
    CHECK_EQ(m::round(-14.0), -14.0);

    // Check for no-op on integers
    CHECK_EQ(m::round(5), 5);
    CHECK_EQ(m::round(-5), -5);

    {
      using T  = double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max *= 2;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max /= 4;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::round(max), max);
    }

    {
      using T  = float;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max *= 2;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max /= 4;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::round(max), max);
    }

#ifndef EMSCRIPTEN
    {
      using T  = long double;
      auto max = static_cast<T>(static_cast<uint64_t>(1) << static_cast<uint64_t>(std::numeric_limits<T>::digits - 1));
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max *= 2;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max /= 4;
      CHECK_EQ(m::round(max), max);
      CHECK_EQ(m::round(-max), -max);

      max = 5721421830524369.0;
      CHECK_EQ(m::round(max), max);
    }
#endif
  }

  TEST_CASE("Date") {
    SUBCASE("Compare") {
      const auto low  = c::Date{3};
      const auto mid  = c::Date{4};
      const auto high = c::Date{5};

      REQUIRE(low < mid);
      REQUIRE((low < low) == false);
      REQUIRE(mid < high);
      REQUIRE(low < high);
      REQUIRE(mid > low);
      REQUIRE((mid > mid) == false);
      REQUIRE(high > mid);
      REQUIRE(high > low);
      REQUIRE((mid < low) == false);
      REQUIRE((high < low) == false);
      REQUIRE((low > mid) == false);
      REQUIRE((low > high) == false);
      REQUIRE(low == low);
      REQUIRE(low != mid);
      REQUIRE(low <= low);
      REQUIRE(low <= mid);
      REQUIRE(mid >= mid);
      REQUIRE(mid >= low);
    }

    SUBCASE("Add") {
      REQUIRE(c::Date{3}.add_days(4) == c::Date{7});
      REQUIRE(c::Date{3}.add_days(static_cast<int64_t>(4)) == c::Date{7});
      REQUIRE(c::Date{c::Date{3}}.add_days(4) == c::Date{c::Date{7}});
      REQUIRE(c::Date{c::Date{3}}.add_days(static_cast<int64_t>(4)) == c::Date{c::Date{7}});
    }

    SUBCASE("Subtract") {
      REQUIRE(c::Date{11}.sub_days(4) == c::Date{7});
      REQUIRE(c::Date{11}.sub_days(static_cast<int64_t>(4)) == c::Date{7});
      REQUIRE(c::Date{c::Date{11}}.sub_days(4) == c::Date{c::Date{7}});
      REQUIRE(c::Date{c::Date{11}}.sub_days(static_cast<int64_t>(4)) == c::Date{c::Date{7}});
    }

    SUBCASE("Day Diff") {
      REQUIRE(c::Date{11}.day_difference(c::Date{7}) == 4);
      REQUIRE(c::Date{11}.day_difference(c::Date{8}) == 3);
      REQUIRE(c::Date{c::Date{11}}.day_difference(c::Date{c::Date{7}}) == 4);
      REQUIRE(c::Date{c::Date{11}}.day_difference(c::Date{c::Date{8}}) == 3);
    }
  }
}
