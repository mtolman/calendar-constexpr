#include <doctest.h>

#include "common.h"
#include "src/calendars/islamic_date.h"

TEST_SUITE("Arithmetic Islamic Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::IslamicDate>();
  }

  TEST_CASE("Conversions") {
    calendars::IslamicDate date{};
    calendars::Date        rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::IslamicDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::IslamicDate;
    using M = T::MONTH;
    static std::vector<T> expected = {
        T{-1245, static_cast<M>(12), 9}, T{-813, static_cast<M>(2), 23}, T{-568, static_cast<M>(4), 1},   T{-501, static_cast<M>(4), 6},
        T{-157, static_cast<M>(10), 17}, T{-47, static_cast<M>(6), 3},   T{75, static_cast<M>(7), 13},    T{403, static_cast<M>(10), 5},
        T{489, static_cast<M>(5), 22},   T{586, static_cast<M>(2), 7},   T{637, static_cast<M>(8), 7},    T{687, static_cast<M>(2), 20},
        T{697, static_cast<M>(7), 7},    T{793, static_cast<M>(7), 1},   T{839, static_cast<M>(7), 6},    T{897, static_cast<M>(6), 1},
        T{960, static_cast<M>(9), 30},   T{967, static_cast<M>(5), 27},  T{1058, static_cast<M>(5), 18},  T{1091, static_cast<M>(6), 2},
        T{1128, static_cast<M>(8), 4},   T{1182, static_cast<M>(2), 3},  T{1234, static_cast<M>(10), 10}, T{1255, static_cast<M>(1), 11},
        T{1321, static_cast<M>(1), 21},  T{1348, static_cast<M>(3), 19}, T{1360, static_cast<M>(9), 8},   T{1362, static_cast<M>(4), 13},
        T{1362, static_cast<M>(10), 7},  T{1412, static_cast<M>(9), 13}, T{1416, static_cast<M>(10), 5},  T{1460, static_cast<M>(10), 12},
        T{1518, static_cast<M>(3), 5},
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::IslamicDate::epoch == 227015);
