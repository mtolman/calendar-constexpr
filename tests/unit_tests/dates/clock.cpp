#include <doctest.h>

#include "src/calendars/clock.h"
#include "common.h"
#include "common_test_headers.h"

TEST_SUITE("Clock") {
  TEST_CASE("Clock time") {
    auto time = calendars::Moment{4.500009}.to<calendars::Clock>().time();
    CHECK_IN_RANGE_DEF(time, 0.500009);
  }

  TEST_CASE("Clock to_moment") {
    auto time = calendars::Moment::from(calendars::Clock{12, 0, 0}).datetime;
    CHECK_IN_RANGE(time, 0.5, 0.001);
  }

  TEST_CASE("Clock pieces") {
    auto time = calendars::Clock{11, 34, 22};
    CHECK_EQ(11, time.hour());
    CHECK_EQ(34, time.minute());
    CHECK_EQ(22, time.second());
  }

  TEST_CASE("Clock from_moment") {
    auto clock = calendars::Moment{0.5}.to<calendars::Clock>();
    CHECK_EQ(clock.date.value[ calendars::Clock::DAY ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::HOUR ], 12);
    CHECK_EQ(clock.date.value[ calendars::Clock::MINUTE ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::SECOND ], 0);

    clock = calendars::Moment{4.5}.to<calendars::Clock>();
    CHECK_EQ(clock.date.value[ calendars::Clock::DAY ], 4);
    CHECK_EQ(clock.date.value[ calendars::Clock::HOUR ], 12);
    CHECK_EQ(clock.date.value[ calendars::Clock::MINUTE ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::SECOND ], 0);
  }

  TEST_CASE("Clock nearest_second") {
    auto clock = calendars::Moment{4.5000000000000001}.to<calendars::Clock>().nearest_second();
    CHECK_EQ(clock.date.value[ calendars::Clock::DAY ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::HOUR ], 12);
    CHECK_EQ(clock.date.value[ calendars::Clock::MINUTE ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::SECOND ], 0);

    clock = calendars::Moment{4.500009}.to<calendars::Clock>().nearest_second();
    CHECK_EQ(clock.date.value[ calendars::Clock::DAY ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::HOUR ], 12);
    CHECK_EQ(clock.date.value[ calendars::Clock::MINUTE ], 0);
    CHECK_EQ(clock.date.value[ calendars::Clock::SECOND ], 1);
  }
}

static_assert(std::is_same_v<calendars::Clock::RDate, calendars::RadixDate<0, 3, 24, 60, 60>>);
