#include <doctest.h>

#include "src/calendars/julian_date.h"
#include "common.h"

TEST_SUITE("Julian Date") {
  TEST_CASE("Comparisons") { comp_tests_three_args<calendars::JulianDate>(); }

  TEST_CASE("Conversions") {
    calendars::JulianDate date;
    calendars::Date     rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::JulianDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::JulianDate;

    static std::vector<T> expected = {T{-587, 7, 30}, T{-169, 12, 8}, T{70, 9, 26},   T{135, 10, 3},   T{470, 1, 7},   T{576, 5, 18},  T{694, 11, 7},
                                      T{1013, 4, 19}, T{1096, 5, 18}, T{1190, 3, 16}, T{1240, 3, 3},   T{1288, 3, 26}, T{1298, 4, 20}, T{1391, 6, 4},
                                      T{1436, 1, 25}, T{1492, 3, 31}, T{1553, 9, 9},  T{1560, 2, 24},  T{1648, 5, 31}, T{1680, 6, 20}, T{1716, 7, 13},
                                      T{1768, 6, 8},  T{1819, 7, 21}, T{1839, 3, 15}, T{1903, 4, 6},   T{1929, 8, 12}, T{1941, 9, 16}, T{1943, 4, 6},
                                      T{1943, 9, 24}, T{1992, 3, 4},  T{1996, 2, 12}, T{2038, 10, 28}, T{2094, 7, 5}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::JulianDate::epoch == -1);
