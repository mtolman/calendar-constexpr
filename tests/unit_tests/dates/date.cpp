#include <doctest.h>

#include "src/calendars/date.h"
#include "src/calendars/julian_day.h"
#include "common.h"

TEST_SUITE("Date") {
  TEST_CASE("Comparisons") {
    comp_test_one_arg<calendars::Date>();
    CHECK(calendars::Date{calendars::Date{0}} == calendars::Date{calendars::Date{0}});
    CHECK(calendars::Date{calendars::Date{58}} != calendars::Date{calendars::Date{0}});

    CHECK(calendars::Date{calendars::Date{100}} == calendars::Date{calendars::JulianDay{1721524.5}});
    CHECK(calendars::Date{calendars::Date{100}} != calendars::Date{calendars::JulianDay{1721424.5}});
    CHECK(calendars::Date{calendars::Date{100}} > calendars::Date{calendars::JulianDay{1721424.5}});
    CHECK(calendars::Date{calendars::Date{100}} < calendars::Date{calendars::JulianDay{1721624.5}});

    CHECK(calendars::Date{calendars::Date{100}} >= calendars::Date{calendars::JulianDay{1721524.5}});
    CHECK(calendars::Date{calendars::Date{100}} >= calendars::Date{calendars::JulianDay{1721424.5}});
    CHECK(calendars::Date{calendars::Date{100}} >= calendars::Date{calendars::JulianDay{1721524.5}});
    CHECK(calendars::Date{calendars::Date{100}} <= calendars::Date{calendars::JulianDay{1721624.5}});
  }

  TEST_CASE("Conversions") {
    CHECK_EQ(50 - calendars::Date::epoch, calendars::Date(50).day);
    CHECK_EQ(calendars::Date{4}, calendars::Date{4}.to<calendars::Date>());
  }


  TEST_CASE("Day of week") {
    CHECK_EQ(calendars::Date{0}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::Date{1}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::Date{2}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::Date{3}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::Date{4}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::Date{5}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::Date{6}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);

    CHECK_EQ(calendars::Date{7}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::Date{8}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::Date{9}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::Date{10}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::Date{11}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::Date{12}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::Date{13}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);

    CHECK_EQ(calendars::Date{calendars::Date{7}}.day_of_week(), calendars::DAY_OF_WEEK::SUNDAY);
    CHECK_EQ(calendars::Date{calendars::Date{8}}.day_of_week(), calendars::DAY_OF_WEEK::MONDAY);
    CHECK_EQ(calendars::Date{calendars::Date{9}}.day_of_week(), calendars::DAY_OF_WEEK::TUESDAY);
    CHECK_EQ(calendars::Date{calendars::Date{10}}.day_of_week(), calendars::DAY_OF_WEEK::WEDNESDAY);
    CHECK_EQ(calendars::Date{calendars::Date{11}}.day_of_week(), calendars::DAY_OF_WEEK::THURSDAY);
    CHECK_EQ(calendars::Date{calendars::Date{12}}.day_of_week(), calendars::DAY_OF_WEEK::FRIDAY);
    CHECK_EQ(calendars::Date{calendars::Date{13}}.day_of_week(), calendars::DAY_OF_WEEK::SATURDAY);
  }

  TEST_CASE("M Cycle") {
    CHECK_EQ(calendars::Date{0}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::Date{1}.day_of_m_cycle(5, 0), 1);
    CHECK_EQ(calendars::Date{2}.day_of_m_cycle(5, 0), 2);
    CHECK_EQ(calendars::Date{3}.day_of_m_cycle(5, 0), 3);
    CHECK_EQ(calendars::Date{4}.day_of_m_cycle(5, 0), 4);
    CHECK_EQ(calendars::Date{5}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::Date{6}.day_of_m_cycle(5, 0), 1);

    CHECK_EQ(calendars::Date{0}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::Date{1}.day_of_m_cycle(5, 1), 0);
    CHECK_EQ(calendars::Date{2}.day_of_m_cycle(5, 1), 1);
    CHECK_EQ(calendars::Date{3}.day_of_m_cycle(5, 1), 2);
    CHECK_EQ(calendars::Date{4}.day_of_m_cycle(5, 1), 3);
    CHECK_EQ(calendars::Date{5}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::Date{6}.day_of_m_cycle(5, 1), 0);

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_m_cycle(5, 0), 1);
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_m_cycle(5, 0), 2);
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_m_cycle(5, 0), 3);
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_m_cycle(5, 0), 4);
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_m_cycle(5, 0), 0);
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_m_cycle(5, 0), 1);

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_m_cycle(5, 1), 0);
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_m_cycle(5, 1), 1);
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_m_cycle(5, 1), 2);
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_m_cycle(5, 1), 3);
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_m_cycle(5, 1), 4);
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_m_cycle(5, 1), 0);
  }

  TEST_CASE("Day of week on or before") {
    CHECK_EQ(calendars::Date{0}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{1}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{2}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{3}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{4}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{5}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{6}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week_on_or_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
  }

  TEST_CASE("Day of week before") {
    CHECK_EQ(calendars::Date{0}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{-7});
    CHECK_EQ(calendars::Date{1}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{2}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{3}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{4}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{5}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{6}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{-7}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week_before(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
  }

  TEST_CASE("Day of week on or after") {
    CHECK_EQ(calendars::Date{0}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{1}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{2}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{3}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{4}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{5}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{6}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week_on_or_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
  }

  TEST_CASE("Day of week after") {
    CHECK_EQ(calendars::Date{0}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{1}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{2}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{3}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{4}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{5}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{6}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week_after(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
  }

  TEST_CASE("Day of week nearest") {
    CHECK_EQ(calendars::Date{0}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{1}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{2}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{3}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{0});
    CHECK_EQ(calendars::Date{4}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{5}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});
    CHECK_EQ(calendars::Date{6}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{7});

    CHECK_EQ(calendars::Date{calendars::Date{0}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{0}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.day_of_week_nearest(calendars::DAY_OF_WEEK::SUNDAY), calendars::Date{calendars::Date{7}});
  }

  TEST_CASE("K-day of M Cycle on or before") {
    CHECK_EQ(calendars::Date{-12}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{0}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{1}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{2}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{3}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{4}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{5}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{6}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{-12});

    CHECK_EQ(calendars::Date{calendars::Date{-12}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{0}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.kth_day_of_m_cycle_on_or_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
  }

  TEST_CASE("K-day of M Cycle before") {
    CHECK_EQ(calendars::Date{-12}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-32});
    CHECK_EQ(calendars::Date{0}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{1}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{2}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{3}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{4}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{5}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{6}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{-12});

    CHECK_EQ(calendars::Date{calendars::Date{-12}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-32}});
    CHECK_EQ(calendars::Date{calendars::Date{0}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.kth_day_of_m_cycle_before(5, 20, 3), calendars::Date{calendars::Date{-12}});
  }

  TEST_CASE("K-day of M Cycle after") {
    CHECK_EQ(calendars::Date{-12}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{0}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{1}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{2}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{3}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{4}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{5}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{6}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{8});

    CHECK_EQ(calendars::Date{calendars::Date{-12}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{0}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.kth_day_of_m_cycle_after(5, 20, 3), calendars::Date{calendars::Date{8}});
  }

  TEST_CASE("K-day of M Cycle on or after") {
    CHECK_EQ(calendars::Date{-12}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{0}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{1}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{2}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{3}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{4}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{5}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{6}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{8});

    CHECK_EQ(calendars::Date{calendars::Date{-12}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{0}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.kth_day_of_m_cycle_on_or_after(5, 20, 3), calendars::Date{calendars::Date{8}});
  }

  TEST_CASE("K-day of M Cycle nearest") {
    CHECK_EQ(calendars::Date{-12}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{-12});
    CHECK_EQ(calendars::Date{0}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{1}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{2}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{3}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{4}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{5}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});
    CHECK_EQ(calendars::Date{6}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{8});

    CHECK_EQ(calendars::Date{calendars::Date{-12}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{-12}});
    CHECK_EQ(calendars::Date{calendars::Date{0}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{1}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{2}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{3}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{4}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{5}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
    CHECK_EQ(calendars::Date{calendars::Date{6}}.kth_day_of_m_cycle_nearest_to(5, 20, 3), calendars::Date{calendars::Date{8}});
  }

  TEST_CASE("0th Nth Weekday") {
    auto rd   = calendars::Date{38};
    CHECK_EQ(rd, rd.nth_week_day(0, calendars::DAY_OF_WEEK::WEDNESDAY));
  }

  TEST_CASE("positions_in_range") {
    auto result = calendars::Date::positions_in_range(3, 7, 2, calendars::Date{calendars::Date{12}}, calendars::Date{calendars::Date{33}});
    CHECK_EQ(result.size(), 3);

    CHECK_EQ(result[ 0 ], calendars::Date{calendars::Date{15}});
    CHECK_EQ(result[ 1 ], calendars::Date{calendars::Date{22}});
    CHECK_EQ(result[ 2 ], calendars::Date{calendars::Date{29}});

    result = calendars::Date::positions_in_range(3, 7, 0, calendars::Date{calendars::Date{12}}, calendars::Date{calendars::Date{33}});
    CHECK_EQ(result.size(), 3);

    CHECK_EQ(result[ 0 ], calendars::Date{calendars::Date{17}});
    CHECK_EQ(result[ 1 ], calendars::Date{calendars::Date{24}});
    CHECK_EQ(result[ 2 ], calendars::Date{calendars::Date{31}});
  }

  TEST_CASE("Day of Week - Sample Data") {
    using T = calendars::DAY_OF_WEEK;

    static std::vector<calendars::DAY_OF_WEEK> expected = {
        T::SUNDAY,    T::WEDNESDAY, T::WEDNESDAY, T::SUNDAY, T::WEDNESDAY, T::MONDAY,    T::SATURDAY, T::SUNDAY,   T::SUNDAY,
        T::FRIDAY,    T::SATURDAY,  T::FRIDAY,    T::SUNDAY, T::SUNDAY,    T::WEDNESDAY, T::SATURDAY, T::SATURDAY, T::SATURDAY,
        T::WEDNESDAY, T::SUNDAY,    T::FRIDAY,    T::SUNDAY, T::MONDAY,    T::WEDNESDAY, T::SUNDAY,   T::SUNDAY,   T::MONDAY,
        T::MONDAY,    T::THURSDAY,  T::TUESDAY,   T::SUNDAY, T::WEDNESDAY, T::SUNDAY};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].day_of_week(), expected[ i ]);
    }
  }
}

static_assert(calendars::Date::epoch == 0);
