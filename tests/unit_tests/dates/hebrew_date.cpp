#include <doctest.h>

#include "src/calendars/hebrew_date.h"
#include "common.h"

TEST_SUITE("Hebrew Date") {
  TEST_CASE("Comparison") {
    comp_tests_three_args<calendars::HebrewDate>();
  }

  TEST_CASE("Conversions") {
    calendars::HebrewDate date{};
    calendars::Date     rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::HebrewDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::HebrewDate;
    using M = T::MONTH;

    static std::vector<T> expected = {
        T{3174, static_cast<M>(5), 10},  T{3593, static_cast<M>(9), 25},  T{3831, static_cast<M>(7), 3},  T{3896, static_cast<M>(7), 9},
        T{4230, static_cast<M>(10), 18}, T{4336, static_cast<M>(3), 4},   T{4455, static_cast<M>(8), 13}, T{4773, static_cast<M>(2), 6},
        T{4856, static_cast<M>(2), 23},  T{4950, static_cast<M>(1), 7},   T{5000, static_cast<M>(13), 8}, T{5048, static_cast<M>(1), 21},
        T{5058, static_cast<M>(2), 7},   T{5151, static_cast<M>(4), 1},   T{5196, static_cast<M>(11), 7}, T{5252, static_cast<M>(1), 3},
        T{5314, static_cast<M>(7), 1},   T{5320, static_cast<M>(12), 27}, T{5408, static_cast<M>(3), 20}, T{5440, static_cast<M>(4), 3},
        T{5476, static_cast<M>(5), 5},   T{5528, static_cast<M>(4), 4},   T{5579, static_cast<M>(5), 11}, T{5599, static_cast<M>(1), 12},
        T{5663, static_cast<M>(1), 22},  T{5689, static_cast<M>(5), 19},  T{5702, static_cast<M>(7), 8},  T{5703, static_cast<M>(1), 14},
        T{5704, static_cast<M>(7), 8},   T{5752, static_cast<M>(13), 12}, T{5756, static_cast<M>(12), 5}, T{5799, static_cast<M>(8), 12},
        T{5854, static_cast<M>(5), 5}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      auto actual = sampleRdDates[ i ].to<T>();
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::HebrewDate::epoch == -1373427);
