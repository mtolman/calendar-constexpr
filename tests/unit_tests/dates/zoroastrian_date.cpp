#include <doctest.h>

#include "src/calendars/zoroastrian_date.h"
#include "common.h"
#include <iostream>

TEST_SUITE("Zoroastrian Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::ZoroastrianDate>();
  }

  TEST_CASE("Conversions") {
    calendars::ZoroastrianDate date;
    calendars::Date          rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::ZoroastrianDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }
}

static_assert(calendars::ZoroastrianDate::epoch == 230638);
