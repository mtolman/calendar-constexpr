#include <doctest.h>

#include "src/calendars/gregorian_date.h"
#include "common.h"

TEST_SUITE("Gregorian Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::GregorianDate>();
  }

  TEST_CASE("Conversions") {
    calendars::GregorianDate date;
    calendars::Date        rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::GregorianDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Ranges") {
    using G = calendars::GregorianDate;
    SUBCASE("Year Start") {
      CHECK_EQ(G{2020, G::MONTH::JANUARY, 1}, G::year_start(2020).to<G>());
      CHECK_EQ(G{-2020, G::MONTH::JANUARY, 1}, G::year_start(-2020).to<G>());
    }

    SUBCASE("Year End") {
      CHECK_EQ(G{2020, G::MONTH::DECEMBER, 31}, G::year_end(2020).to<G>());
      CHECK_EQ(G{-2020, G::MONTH::DECEMBER, 31}, G::year_end(-2020).to<G>());
    }

    SUBCASE("Year Boundaries") {
      CHECK_EQ(G{2020, G::MONTH::JANUARY, 1}, std::get<0>(G::year_boundaries(2020)).to<G>());
      CHECK_EQ(G{2020, G::MONTH::DECEMBER, 31}, std::get<1>(G::year_boundaries(2020)).to<G>());
    }

    SUBCASE("Year Vector") {
      SUBCASE("Non-leap year") {
        const auto year = 2019;
        REQUIRE(!G::is_leap_year(year));
        auto v = G::year_vector(year);
        CHECK_EQ(365, v.size());
        CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
        CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

        for (size_t i = 1; i < v.size(); ++i) {
          CHECK_EQ(v[ i ].day_difference(v[ i - 1 ]), 1);
        }
      }

      SUBCASE("leap year") {
        const auto year = 2020;
        REQUIRE(G::is_leap_year(year));
        auto v = G::year_vector(year);
        CHECK_EQ(366, v.size());
        CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
        CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

        for (size_t i = 1; i < v.size(); ++i) {
          CHECK_EQ(v[ i ].day_difference(v[ i - 1 ]), 1);
        }
      }

      SUBCASE("Days in Year") {
        auto check = [](int32_t year, int16_t month, uint16_t day) {
          auto date = calendars::GregorianDate(year, month, day);
          ;
          const auto expected = calendars::GregorianDate::is_leap_year(year) ? 366 : 365;
          REQUIRE_EQ(date.day_number() + date.days_remaining(), expected);
          REQUIRE((date.day_number() != date.days_remaining() || (expected == 366 && date.day_number() == 366 / 2)));
        };

        check(2020, 12, 28);
        check(-256, 3, 2);
        check(4, 2, 29);
      }

      SUBCASE("Year Range Constexpr") {
        SUBCASE("Non-leap") {
          constexpr auto year = 2019;
          const auto     v    = G::year_range_constexpr<year>();
          REQUIRE_EQ(v.size(), 365);
          CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
          CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

          for (size_t i = 1; i < v.size(); ++i) {
            CHECK_EQ(v.at(i).day_difference(v.at(i - 1)), 1);
          }
        }

        SUBCASE("Leap") {
          constexpr auto year = 2020;
          const auto     v    = G::year_range_constexpr<year>();
          REQUIRE_EQ(v.size(), 366);
          CHECK_EQ(G{year, G::MONTH::JANUARY, 1}, v[ 0 ].to<G>());
          CHECK_EQ(G{year, G::MONTH::DECEMBER, 31}, v[ v.size() - 1 ].to<G>());

          for (size_t i = 1; i < v.size(); ++i) {
            CHECK_EQ(v.at(i).day_difference(v.at(i - 1)), 1);
          }
        }
      }
    }
  }
  
  TEST_CASE("Sample Data") {
    using T = calendars::GregorianDate;
    
    static std::vector<T> expected = {T{-586, 7, 24}, T{-168, 12, 5}, T{70, 9, 24},   T{135, 10, 2},   T{470, 1, 8},   T{576, 5, 20},  T{694, 11, 10},
                                      T{1013, 4, 25}, T{1096, 5, 24}, T{1190, 3, 23}, T{1240, 3, 10},  T{1288, 4, 2},  T{1298, 4, 27}, T{1391, 6, 12},
                                      T{1436, 2, 3},  T{1492, 4, 9},  T{1553, 9, 19}, T{1560, 3, 5},   T{1648, 6, 10}, T{1680, 6, 30}, T{1716, 7, 24},
                                      T{1768, 6, 19}, T{1819, 8, 2},  T{1839, 3, 27}, T{1903, 4, 19},  T{1929, 8, 25}, T{1941, 9, 29}, T{1943, 4, 19},
                                      T{1943, 10, 7}, T{1992, 3, 17}, T{1996, 2, 25}, T{2038, 11, 10}, T{2094, 7, 18}};

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::GregorianDate::epoch == 1);
