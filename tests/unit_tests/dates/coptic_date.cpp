#include <doctest.h>

#include "src/calendars/coptic_date.h"
#include "common.h"

TEST_SUITE("Coptic Date") {
  TEST_CASE("Comparisons") {
    comp_tests_three_args<calendars::CopticDate>();
  }

  TEST_CASE("Conversions") {
    calendars::CopticDate date;
    calendars::Date     rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::CopticDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::CopticDate;

    static std::vector<T> expected = {
        T{-870, 12, 6},
        T{-451, 4, 12},
        T{-213, 1, 29},
        T{-148, 2, 5},
        T{186, 5, 12},
        T{292, 9, 23},
        T{411, 3, 11},
        T{729, 8, 24},
        T{812, 9, 23},
        T{906, 7, 20},
        T{956, 7, 7},
        T{1004, 7, 30},
        T{1014, 8, 25},
        T{1107, 10, 10},
        T{1152, 5, 29},
        T{1208, 8, 5},
        T{1270, 1, 12},
        T{1276, 6, 29},
        T{1364, 10, 6},
        T{1396, 10, 26},
        T{1432, 11, 19},
        T{1484, 10, 14},
        T{1535, 11, 27},
        T{1555, 7, 19},
        T{1619, 8, 11},
        T{1645, 12, 19},
        T{1658, 1, 19},
        T{1659, 8, 11},
        T{1660, 1, 26},
        T{1708, 7, 8},
        T{1712, 6, 17},
        T{1755, 3, 1},
        T{1810, 11, 11}
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::CopticDate::epoch == 103605);
