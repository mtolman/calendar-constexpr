#include <doctest.h>

#include "src/calendars/icelandic_date.h"
#include "common.h"

TEST_SUITE("Icelandic Dates") {
  TEST_CASE("Comparisons") {
    using CLASS = calendars::IcelandicDate;

    CHECK(CLASS{0, 0, 0, 0} == CLASS{0, 0, 0, 0});
    CHECK(CLASS{58, 90, 2, 0} == CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{5, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{12, 180, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{12, 0, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{12, 90, 6, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{12, 90, 0, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} == CLASS{12, 90, 2, 1});

    CHECK_FALSE(CLASS{58, 90, 2, 0} != CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{5, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{12, 180, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{12, 0, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{12, 90, 6, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{12, 90, 0, 0});
    CHECK(CLASS{12, 90, 2, 0} != CLASS{12, 90, 2, 1});

    CHECK_FALSE(CLASS{0, 0, 0, 0} < CLASS{0, 0, 0, 0});
    CHECK_FALSE(CLASS{58, 90, 2, 0} < CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} < CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} < CLASS{12, 180, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} < CLASS{12, 90, 6, 0});
    CHECK(CLASS{12, 90, 2, 0} < CLASS{12, 90, 2, 1});
    CHECK_FALSE(CLASS{12, 90, 2, 0} < CLASS{12, 0, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} < CLASS{12, 90, 0, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} < CLASS{5, 90, 2, 0});
    CHECK_FALSE(CLASS{15, 0, 2, 0} < CLASS{13, 90, 2, 0});
    CHECK_FALSE(CLASS{15, 180, 1, 0} < CLASS{13, 90, 2, 0});
    CHECK_FALSE(CLASS{13, 90, 2, 1} < CLASS{13, 90, 2, 0});

    CHECK_FALSE(CLASS{0, 0, 0, 0} > CLASS{0, 0, 0, 0});
    CHECK_FALSE(CLASS{58, 90, 2, 0} > CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} > CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} > CLASS{12, 180, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} > CLASS{12, 90, 6, 0});
    CHECK(CLASS{12, 90, 2, 0} > CLASS{12, 0, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} > CLASS{12, 90, 0, 0});
    CHECK(CLASS{12, 90, 2, 0} > CLASS{5, 90, 2, 0});

    CHECK(CLASS{0, 0, 0, 0} <= CLASS{0, 0, 0, 0});
    CHECK(CLASS{58, 90, 2, 0} <= CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} <= CLASS{58, 90, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} <= CLASS{12, 180, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} <= CLASS{12, 90, 6, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} <= CLASS{12, 0, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} <= CLASS{12, 90, 0, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} <= CLASS{5, 90, 2, 0});

    CHECK(CLASS{0, 0, 0, 0} >= CLASS{0, 0, 0, 0});
    CHECK(CLASS{58, 90, 2, 0} >= CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} >= CLASS{58, 90, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} >= CLASS{12, 180, 2, 0});
    CHECK_FALSE(CLASS{12, 90, 2, 0} >= CLASS{12, 90, 6, 0});
    CHECK(CLASS{12, 90, 2, 0} >= CLASS{12, 0, 2, 0});
    CHECK(CLASS{12, 90, 2, 0} >= CLASS{12, 90, 0, 0});
    CHECK(CLASS{12, 90, 2, 0} >= CLASS{5, 90, 2, 0});
  }

  TEST_CASE("Conversions") {
    calendars::IcelandicDate date{};
    calendars::Date        rdate;

    rdate.day = 8849;
    date      = rdate.to<calendars::IcelandicDate>();
    rdate     = calendars::Date::from(date);
    CHECK_EQ(rdate.day, 8849);
  }

  TEST_CASE("Sample Data") {
    using T = calendars::IcelandicDate;
    using S = calendars::IcelandicDate::SEASON;
    using D = calendars::DAY_OF_WEEK;

    static std::vector<T> expected = {
        T{-587, S::WINTER, 22, D::SUNDAY},   T{-168, S::SUMMER, 15, D::WEDNESDAY}, T{70, S::SUMMER, 22, D::WEDNESDAY},
        T{135, S::SUMMER, 24, D::SUNDAY},    T{469, S::WINTER, 11, D::WEDNESDAY},  T{576, S::SUMMER, 4, D::MONDAY},
        T{694, S::WINTER, 3, D::SATURDAY},   T{1013, S::SUMMER, 1, D::SUNDAY},     T{1096, S::SUMMER, 5, D::SUNDAY},
        T{1189, S::WINTER, 22, D::FRIDAY},   T{1239, S::WINTER, 21, D::SATURDAY},  T{1287, S::WINTER, 23, D::FRIDAY},
        T{1298, S::SUMMER, 1, D::SUNDAY},    T{1391, S::SUMMER, 8, D::SUNDAY},     T{1435, S::WINTER, 15, D::WEDNESDAY},
        T{1491, S::WINTER, 25, D::SATURDAY}, T{1553, S::SUMMER, 22, D::SATURDAY},  T{1559, S::WINTER, 20, D::SATURDAY},
        T{1648, S::SUMMER, 7, D::WEDNESDAY}, T{1680, S::SUMMER, 10, D::SUNDAY},    T{1716, S::SUMMER, 14, D::FRIDAY},
        T{1768, S::SUMMER, 9, D::SUNDAY},    T{1819, S::SUMMER, 15, D::MONDAY},    T{1838, S::WINTER, 22, D::WEDNESDAY},
        T{1902, S::WINTER, 26, D::SUNDAY},   T{1929, S::SUMMER, 18, D::SUNDAY},    T{1941, S::SUMMER, 23, D::MONDAY},
        T{1942, S::WINTER, 26, D::MONDAY},   T{1943, S::SUMMER, 25, D::THURSDAY},  T{1991, S::WINTER, 21, D::TUESDAY},
        T{1995, S::WINTER, 18, D::SUNDAY},   T{2038, S::WINTER, 3, D::WEDNESDAY},  T{2094, S::SUMMER, 13, D::SUNDAY},
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      auto actual = sampleRdDates[ i ].to<T>();
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::IcelandicDate::epoch == 109);
