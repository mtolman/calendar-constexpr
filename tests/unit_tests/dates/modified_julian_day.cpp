#include <doctest.h>

#include "src/calendars/modified_julian_day.h"
#include "common.h"
#include "common_test_headers.h"

TEST_SUITE("Modified Julian Day") {
  TEST_CASE("Comparisons") { comp_test_one_arg<calendars::ModifiedJulianDay>(); }

  TEST_CASE("Conversions") {
    calendars::ModifiedJulianDay date{};
    calendars::Date            rdate{};

    rdate.day     = 0;
    date.datetime = -678576;
    CHECK_EQ(rdate.day, calendars::Date::from(date).day);
    CHECK_IN_RANGE(date.datetime, rdate.to<calendars::ModifiedJulianDay>().datetime, 0.01)

    rdate.day     = 98532;
    date.datetime = -580044;
    CHECK_EQ(rdate.day, calendars::Date::from(date).day);
    CHECK_IN_RANGE(date.datetime, rdate.to<calendars::ModifiedJulianDay>().datetime, 0.01)

    calendars::Moment expected{0.0};
    expected.datetime = 0.0;
    date.datetime     = -calendars::ModifiedJulianDay::epoch;

    CHECK_EQ(date.to_moment(), expected);
    CHECK_EQ(calendars::ModifiedJulianDay::from_moment(expected), date);
    CHECK_IN_RANGE(calendars::Moment::from(date).datetime, expected.datetime, 0.01)
    CHECK_IN_RANGE(date.datetime, expected.to<calendars::ModifiedJulianDay>().datetime, 0.01);
    CHECK_IN_RANGE(rdate.to<calendars::ModifiedJulianDay>().datetime, -580044.0, 0.01);

    expected.datetime = -9965.5;
    date.datetime     = -calendars::ModifiedJulianDay::epoch - 9965.5;
    CHECK_IN_RANGE(expected.datetime, calendars::Moment::from(date).datetime, 0.01)
    CHECK_IN_RANGE(date.datetime, expected.to<calendars::ModifiedJulianDay>().datetime, 0.01)
  }

  TEST_CASE("Sample Data") {
    using T = calendars::ModifiedJulianDay;

    static std::vector<T> expected = {
        T{-892769}, T{-739963}, T{-653107}, T{-629359}, T{-507269}, T{-468421}, T{-425149}, T{-308836}, T{-278491}, T{-244221}, T{-225971},
        T{-208416}, T{-204739}, T{-170726}, T{-154420}, T{-133900}, T{-111458}, T{-109099}, T{-76860},  T{-65152},  T{-51980},  T{-33022},
        T{-14352},  T{-7175},   T{16223},   T{25848},   T{30266},   T{30833},   T{31004},   T{48698},   T{50138},   T{65737},   T{86076},
    };

    CHECK_EQ(expected.size(), sampleRdDates.size());

    for (size_t i = 0; i < expected.size(); ++i) {
      CHECK_EQ(sampleRdDates[ i ].to<T>(), expected[ i ]);
      CHECK_EQ(calendars::Date::from(expected[ i ]), sampleRdDates[ i ]);
    }
  }
}

static_assert(calendars::ModifiedJulianDay::epoch == 678576);
