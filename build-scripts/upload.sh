NOW=$(date +"%Y-%m-%dT%H-%M-%S-%s")
VERSION_COMMIT=$(git rev-parse HEAD)
VERSION="$NOW/$VERSION_COMMIT"

if [ -z "$UPLOAD_HOST" ]; then
	echo "env variable UPLOAD_HOST not set, unable to upload!"
	exit 1
fi

if [ -z "$USER_CREDS" ]; then
	echo "env variable USER_CREDS not set, unable to upload!"
	exit 1
fi

cd lib
for f in *
do
	echo "Uploading ${f} with version ${VERSION} to ${UPLOAD_HOST}/cpp/calendar/lib/${VERSION}/${f}"
	curl -v -u $USER_CREDS --upload-file ${f} "${UPLOAD_HOST}/cpp/calendar/lib/${VERSION}/${f}"
done

cd ../doxygen-out
for f in *
do
	echo "Uploading ${f} with version ${VERSION} to ${UPLOAD_HOST}/cpp/calendar/docs/${VERSION}/${f}"
	curl -v -u $USER_CREDS --upload-file ${f} "${UPLOAD_HOST}/cpp/calendar/docs/${VERSION}/${f}"
done

exit 0
