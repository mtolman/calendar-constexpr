#!/usr/bin/env bash

THREADS=$(grep ^cpu\\scores /proc/cpuinfo | uniq |  awk '{print $4}')
THREADS=$(($THREADS * 2))

mkdir -p test-out
rm -rf test-out/CMakeCache.txt
cd test-out || exit 1
cmake .. \
  -DCMAKE_BUILD_TYPE=Debug \
  -G "Unix Makefiles"
make -j "$THREADS" && make test
