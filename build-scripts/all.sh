#!/usr/bin/env bash

bash build-scripts/format-check.sh && \
bash build-scripts/clang-tidy.sh && \
bash build-scripts/ctest.sh && \
bash build-scripts/coverage.sh && \
bash build-scripts/sanitize.sh && \
bash build-scripts/docs.sh
