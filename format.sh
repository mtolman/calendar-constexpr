#!/usr/bin/env bash
#clang-format --version
clang-format -i -style=file unit_tests/* src/*.h src/*.cpp prop_tests/* deps/common_test_headers.h deps/variant_visit.h